// projection.cc

/********************************************************
 * Make projections of 3D data.
 ********************************************************/

#include "projection.h"

/*
    Define image x and y vectors in datacube coordinates based on the norm
    vector given. Norm vector is directed at the viewer along the im_z axis
    (right-handed).

    Rules:

    1) If n not // to z, we see a projection of a polar coordinate system
       with z-axis standing vertically. im_x = z ^ n, im_y = n ^ im_x (^
       means cross-product).

    2) If n // z, then align x-axis with image x-axis. im_y = n ^ x, im_x
       = x; only two cases -- looking along z (z points away from us) in
       which case our image axes are (x, -y). im_x = x, im_y = -y; looking
       into z (z points towards us), in which case our image axes are (x,
       y). im_x = x, im_y = y.
    */
void calculate_image_basis_vectors(double *imax_x, double *imax_y, double *n) {
    if (n[0] == 0 && n[1] == 0) // n parallel to z
    {
        imax_x[0] = 1;  // (1, 0, 0)
        if (n[2] > 0) // don't compare with 1 or -1 since it's floating point
        {  // z points towards us
            imax_y[1] = 1; // (0, 1, 0)
        }
        else
        {  // z points away from us
            imax_y[1] = -1; // (0, -1, 0)
        }
    }
    else // n not parallel to z, see polar system
    {
        imax_y[2] = 1; // imax_y is temporarily our (0, 0, 1) basis vector
        cross(imax_y, n, imax_x); // imax_x = (0,0,1) x n
        double imax_x_norm = norm(imax_x, 3);
        for (int i=0; i < 3; ++i) imax_x[i] /= imax_x_norm; // Normalize imax_x
        cross(n, imax_x, imax_y); // imax_x, n are orthonormal so imax_y is unit
    }
}


/*
    Define image x and y vectors in datacube coordinates based on the two norm
    vectors given. Norm vector of projection is directed at the viewer along
    the im_z axis (right-handed).

    n and ns should be normalized first.

    For the image slice, x-axis is along the LOS direction, increasing with
    distance away, so imax_x = - n. y-axis is in the sky plane, and to emulate
    a 90 degree rotation from viewing the projection, to viewing the slice
    head-on, take imax_y = n x ns.

    What if n and ns are not at right angles? So long as they are not
    parallel, the two image axes will always be perpendicular, one along the
    LOS and the other in the sky plane. In effect, only the sky plane
    component of ns is used. However, n and ns must not be parallel.
    */
void calculate_slice_basis_vectors(double *imax_x, double *imax_y,
    double *n, double *ns) {
    double ndotns = dot(n, ns);
    if (1.0 - ndotns < 1e-2) // n too parallel to ns
    {
        std::cout << "Warning: the normal vectors must not be parallel." << std::endl;
    }
    else // n not parallel to z, see polar system
    {
        for (int i=0; i < 3; ++i) imax_x[i] = -n[i];  // imax_x = - n
        cross(n, ns, imax_y);  // imax_y = n x ns
    }
}


/*
 * Find image offset and image size by projecting the 8 corners.
 */
void calculate_image_offset(
    long *imoff_x, long *imoff_y, long *imsize_x, long *imsize_y,
    bool crop_image, double binning, double *imax_x, double *imax_y,
    double *cube_center, double cube_r,
    double cube_xmin, double cube_xmax,
    double cube_ymin, double cube_ymax,
    double cube_zmin, double cube_zmax) {

    if (!crop_image) {
        // Without image cropping -- find the size of the entire
        // projection, and the lower left corner (0, 0) position of the
        // projected image relative to the projected position of the
        // datacube origin.
        double c1[] = {cube_xmin, cube_ymin, cube_zmin};
        double c2[] = {cube_xmax, cube_ymin, cube_zmin};
        double c3[] = {cube_xmin, cube_ymax, cube_zmin};
        double c4[] = {cube_xmin, cube_ymin, cube_zmax};
        double c5[] = {cube_xmax, cube_ymax, cube_zmin};
        double c6[] = {cube_xmax, cube_ymin, cube_zmax};
        double c7[] = {cube_xmin, cube_ymax, cube_zmax};
        double c8[] = {cube_xmax, cube_ymax, cube_zmax};

        double corner_x[] = {
            dot(imax_x,c1), dot(imax_x,c2), dot(imax_x,c3), dot(imax_x,c4),
            dot(imax_x,c5), dot(imax_x,c6), dot(imax_x,c7), dot(imax_x,c8)
        };
        double corner_y[] = {
            dot(imax_y,c1), dot(imax_y,c2), dot(imax_y,c3), dot(imax_y,c4),
            dot(imax_y,c5), dot(imax_y,c6), dot(imax_y,c7), dot(imax_y,c8)
        };

        // Offset of the lower left image corner relative to the projected
        // coordinates of the datacube origin
        *imoff_x  = (long) floor(*std::min_element(corner_x, corner_x+8));
        *imsize_x = (long) (ceil(*std::max_element(corner_x, corner_x+8)) -
                            (*imoff_x));
        *imoff_y  = (long) floor(*std::min_element(corner_y, corner_y+8));
        *imsize_y = (long) (ceil(*std::max_element(corner_y, corner_y+8)) -
                            (*imoff_y));
    }
    else {
        // With image cropping -- image size is twice the specified
        // radius, and the lower left corner of the image is calculated
        // relative to the position of the projection of datacube's
        // origin.
        *imoff_x = (long) (dot(imax_x, cube_center) - cube_r/binning);
        *imsize_x = (long) ceil(2*cube_r/binning);
        *imoff_y = (long) (dot(imax_y, cube_center) - cube_r/binning);
        *imsize_y = (*imsize_x);
    }
}


/*
 * Calculate column length for correct normalization of the LOS integration
 */
void calculate_column_length(
    long imoff_x, long imoff_y, long imsize_x, long imsize_y,
    long ax1, long ax2, long ax3,
    double *imax_x, double *imax_y, double *n, double binning,
    std::valarray<double> *projected_column) {

    std::cout << "+----------------------------------------------------------------------+" << std::endl;
    std::cout << "calculate_column_length():" << std::endl;
    std::cout << "imoff_x\timoff_y\timsize_x\timsize_y" << std::endl;
    std::cout << imoff_x << "\t" << imoff_y << "\t" << imsize_x << "\t" << imsize_y << std::endl;
    std::cout << "ax1\tax2\tax3" << std::endl;
    std::cout << ax1 << "\t" << ax2 << "\t" << ax3 << std::endl;
    std::cout << "imax_x[0]\timax_y[0]\tn[0]\tbinning" << std::endl;
    std::cout << *imax_x << "\t" <<  *imax_y << "\t" << *n << "\t" << binning << std::endl;
    std::cout << "&projected_column\t" << &projected_column << std::endl;
    std::cout << "+----------------------------------------------------------------------+" << std::endl;

    double Ix0, Iy0, Iz0, Ix, Iy, Iz, Iyx, Iyy, Iyz, \
           l, cutx, cuty, cutz, found_l[2] = {0.0, 0.0};
    long count_l;

    // Image (0, 0) in datacube coords
    // binning * (offset_x * image_x_axis + offset_y * image_y_axis)
    // Multiply by binning**2 if image_axis have been divided by binning...
    Ix0 = imoff_x * imax_x[0] + imoff_y * imax_y[0];
    Iy0 = imoff_x * imax_x[1] + imoff_y * imax_y[1];
    Iz0 = imoff_x * imax_x[2] + imoff_y * imax_y[2];

    for (long imy=0; imy < imsize_y; ++imy) {
        Iyx = Ix0 + (imy+0.5) * imax_y[0];
        Iyy = Iy0 + (imy+0.5) * imax_y[1];
        Iyz = Iz0 + (imy+0.5) * imax_y[2];
        for (long imx=0; imx < imsize_x; ++imx) {
            // Pixel center of projected image -> datacube coordinates
            Ix = (Iyx + (imx+0.5) * imax_x[0])*binning*binning;
            Iy = (Iyy + (imx+0.5) * imax_x[1])*binning*binning;
            Iz = (Iyz + (imx+0.5) * imax_x[2])*binning*binning;

            count_l = 0;

            // Check intersection with x=0
            l = -Ix/n[0];
            cuty = Iy + l * n[1];
            cutz = Iz + l * n[2];
            if (cuty >= 0 && cuty <= ax2 && cutz >= 0 && cutz <= ax3) {
                // Record this l
                found_l[count_l] = l;
                count_l++;
            }

            // Check intersection with x=ax1
            l = (ax1-Ix)/n[0];
            cuty = Iy + l * n[1];
            cutz = Iz + l * n[2];
            if (cuty >= 0 && cuty <= ax2 && cutz >= 0 && cutz <= ax3) {
                // Record this l
                found_l[count_l] = l;
                count_l++;
            }

            if (count_l < 2) {
                // Check intersection with y=0
                l = -Iy/n[1];
                cutx = Ix + l * n[0];
                cutz = Iz + l * n[2];
                if (cutx >= 0 && cutx <= ax1 && cutz >= 0 && cutz <= ax3) {
                    // Record this l
                    found_l[count_l] = l;
                    count_l++;
                }
            }

            if (count_l < 2) {
                // Check intersection with y=ax2
                l = (ax2-Iy)/n[1];
                cutx = Ix + l * n[0];
                cutz = Iz + l * n[2];
                if (cutx >= 0 && cutx <= ax1 && cutz >= 0 && cutz <= ax3) {
                    // Record this l
                    found_l[count_l] = l;
                    count_l++;
                }
            }

            if (count_l < 2) {
                // Check intersection with z=0
                l = -Iz/n[2];
                cutx = Ix + l * n[0];
                cuty = Iy + l * n[1];
                if (cutx >= 0 && cutx <= ax1 && cuty >= 0 && cuty <= ax2) {
                    // Record this l
                    found_l[count_l] = l;
                    count_l++;
                }
            }

            if (count_l < 2) {
                // Check intersection with z=ax3
                l = (ax3-Iz)/n[2];
                cutx = Ix + l * n[0];
                cuty = Iy + l * n[1];
                if (cutx >= 0 && cutx <= ax1 && cuty >= 0 && cuty <= ax2) {
                    // Record this l
                    found_l[count_l] = l;
                    count_l++;
                }
            }

            if (count_l == 2) {
                (*projected_column)[imy*imsize_x+imx] =
                    std::abs(found_l[0] - found_l[1]);
                //std::cout << imx << "\t" << imy << " distance: " << found_l[0]-found_l[1] << std::endl;
            }
            else {
                //std::cout << imx << "\t" << imy << " no intersection (" << count_l << ")" << std::endl;
            }
        }
    }
}


/*
 * Drizzle datacube onto image pixels, by transforming datacube pixel
 * coordinates to image pixel coordinates.
 *
 * Parallel ops:
 * Shared writes -- counter, projected_img, projected_count
 * Shared read -- contents
 *
 * contents, projected_img, projected_count -- data arrays, pass by reference
 * imax_x, imax_y -- array(3) with image axes vectors
 * ax1, ax2, ax3 -- (x, y, z) dimensions of contents
 * imsize_x, imsize_y -- dimensions of output image
 * imoff_x, imoff_y -- computed image offsets for the transformation
 * dither_x, dither_y, dither_z -- location of pixel center, between 0 and 1,
 *      for the projection (e.g. for no dither, drizzle once using value of 0.5)
 */
void drizzle(std::valarray<float> *contents,
            std::valarray<double> *projected_img,
            std::valarray<long> *projected_count,
            double *imax_x, double *imax_y,
            long ax1, long ax2, long ax3,
            long imsize_x, long imsize_y, long imoff_x, long imoff_y,
            double dither_x, double dither_y, double dither_z,
            int subsample) {

    // Dot product calc
    double zx[ax3], zy[ax3], yx[ax2], yy[ax2], xx[ax1], xy[ax1];
    long counter = 0; // Progress counter

    // Pre-calculate parts of dot product, [dither + (x, y, z)] . im_axis
    for (long z=0; z<ax3; ++z) {
        zx[z] = (z + dither_z) * imax_x[2]; // z part of dot product, for this z
        zy[z] = (z + dither_z) * imax_y[2];
    }
    for (long y=0; y<ax2; ++y) {
        yx[y] = (y + dither_y) * imax_x[1]; // Add the y part of the dot product
        yy[y] = (y + dither_y) * imax_y[1];
    }
    for (long x=0; x<ax1; ++x) {
        xx[x] = (x + dither_x) * imax_x[0]; // Add the y part of the dot product
        xy[x] = (x + dither_x) * imax_y[0];
    }

    omp_set_num_threads(4);
    #pragma omp parallel for
    for (int z=0; z<ax3; ++z) {
        long iz = z*ax1*ax2, izy, inx, imx, imy, pcounter;
        for (int y=0; y<ax2; ++y) {
            izy = iz+y*ax1;
            for (int x=0;x<ax1;++x) {
                imy = (long)floor(zy[z]+yy[y]+xy[x]) - imoff_y;
                if (imy < 0 || imy >= imsize_y) {
                    continue;
                }
                imx = (long)floor(zx[z]+yx[y]+xx[x]) - imoff_x;
                if (imx < 0 || imx >= imsize_x) {
                    continue;
                }
                inx = imy*imsize_x + imx;
                /////////////////////////////////////////
                #pragma omp atomic update
                (*projected_img)[inx]   += (*contents)[izy+x];
                /////////////////////////////////////////
                #pragma omp atomic update
                (*projected_count)[inx] += 1;
                /////////////////////////////////////////
            }
        }
        //////////////////////////////////
        #pragma omp atomic capture
        pcounter = ++counter;
        //////////////////////////////////
        if (pcounter%10==0) display_progress(
            (float)pcounter/ax3/subsample/subsample/subsample);
    }
}


/*
 * Drizzle datacube onto image pixels, by transforming datacube pixel
 * coordinates to image pixel coordinates.
 *
 * Parallel ops:
 * Shared writes -- counter, projected_img, projected_count
 * Shared read -- contents
 *
 * contents, projected_img, projected_count -- data arrays, pass by reference
 * imax_x, imax_y -- array(3) with image axes vectors
 * ax1, ax2, ax3 -- (x, y, z) dimensions of contents
 * imsize_x, imsize_y -- dimensions of output image
 * imoff_x, imoff_y -- computed image offsets for the transformation
 * dither_x, dither_y, dither_z -- location of pixel center, between 0 and 1,
 *      for the projection (e.g. for no dither, drizzle once using value of 0.5)
 */
void drizzle2(std::valarray<float> *contents,
            std::valarray<double> *projected_img,
            std::valarray<long> *projected_count,
            double *imax_x, double *imax_y,
            long ax1, long ax2, long ax3,
            long imsize_x, long imsize_y, long imoff_x, long imoff_y,
            double dither_x, double dither_y, double dither_z,
            int subsample,
            double *cube_center, double *ns, double thickness) {

    // Dot product calc
    double zx[ax3], zy[ax3], yx[ax2], yy[ax2], xx[ax1], xy[ax1];
    long counter = 0; // Progress counter

    // Pre-calculate parts of dot product, [dither + (x, y, z)] . im_axis
    for (long z=0; z<ax3; ++z) {
        zx[z] = (z + dither_z) * imax_x[2]; // z part of dot product, for this z
        zy[z] = (z + dither_z) * imax_y[2];
    }
    for (long y=0; y<ax2; ++y) {
        yx[y] = (y + dither_y) * imax_x[1]; // Add the y part of the dot product
        yy[y] = (y + dither_y) * imax_y[1];
    }
    for (long x=0; x<ax1; ++x) {
        xx[x] = (x + dither_x) * imax_x[0]; // Add the y part of the dot product
        xy[x] = (x + dither_x) * imax_y[0];
    }

    omp_set_num_threads(4);
    #pragma omp parallel for
    for (int z=0; z<ax3; ++z) {
        double dist_x, dist_y, dist_z;  // Dither position distance from plane
        long iz = z*ax1*ax2, izy, inx, imx, imy, pcounter;
        dist_z = (z - cube_center[2]) * ns[2];
        for (int y=0; y<ax2; ++y) {
            izy = iz+y*ax1;
            dist_y = dist_z + (y - cube_center[1]) * ns[1];
            for (int x=0;x<ax1;++x) {
                imy = (long)floor(zy[z]+yy[y]+xy[x]) - imoff_y;
                if (imy < 0 || imy >= imsize_y) {
                    continue;
                }
                imx = (long)floor(zx[z]+yx[y]+xx[x]) - imoff_x;
                if (imx < 0 || imx >= imsize_x) {
                    continue;
                }
                inx = imy*imsize_x + imx;
                dist_x = dist_y + (x - cube_center[0]) * ns[0];
                if (std::abs(dist_x) <= thickness && (*contents)[izy+x] > 0) {
                    /////////////////////////////////////////
                    #pragma omp atomic update
                    (*projected_img)[inx]   += (*contents)[izy+x];
                    /////////////////////////////////////////
                    #pragma omp atomic update
                    (*projected_count)[inx] += 1;
                    /////////////////////////////////////////
                }
            }
        }
        //////////////////////////////////
        #pragma omp atomic capture
        pcounter = ++counter;
        //////////////////////////////////
        if (pcounter%10==0) display_progress(
            (float)pcounter/ax3/subsample/subsample/subsample);
    }
}
