// datacube.cc

/********************************************************
 * Read data from FITS file and write image to FITS file.
 ********************************************************/

#include "datacube.h"

Datacube::Datacube(char* inputfilename) {
    std::cout << "Reading FITS file " << inputfilename << std::endl;
    initSuccess = 0;

    readcube(inputfilename, naxes, &contents);

    filename = inputfilename;

    // Data is stored [z][y][x]; the FITS axes are [ax3][ax2][ax1]
    ax1 = naxes[0]; //x
    ax2 = naxes[1]; //y
    ax3 = naxes[2]; //z

    std::cout << "Datacube size: " <<
        ax1 << " x " << ax2 << " x " << ax3 << std::endl;

    initSuccess = 1;
}


// Check if data loaded
bool Datacube::CheckInit() {
    return initSuccess;
}


// Get a pointer to data
std::valarray<float> Datacube::GetDataPtr() {
    return contents;
}


// Get axes
void Datacube::GetAxes(long* output) {
    output[0] = ax1;
    output[1] = ax2;
    output[2] = ax3;
}


void printerror( int status)
{
    /*****************************************************/
    /* Print out cfitsio error messages and exit program */
    /*****************************************************/
    if (status)
    {
       fits_report_error(stderr, status); /* print error report */

       //exit( status );    /* terminate the program, returning error status */
    }
    return;
}

/*
 * Must take the valarray pointer by reference, because the .resize() call
 * changes the pointed-to memory location.
 */
void readcube(char* filename, long* naxes, std::valarray<float> *data) {
    fitsfile *fptr;       /* pointer to the FITS file, defined in fitsio.h */
    int status,  nfound, anynull;
    long fpixel, nbuffer, npixels, ii;

    #define buffsize 1000
    #define AXMAX 1024  // Max axis size
    float nullval, buffer[buffsize];

    status = 0;

    if ( fits_open_file(&fptr, filename, READONLY, &status) )
         printerror( status );

    /* read the NAXIS1 and NAXIS2 keyword to get image size */
    if ( fits_read_keys_lng(fptr, "NAXIS", 1, 3, naxes, &nfound, &status) )
         printerror( status );

    if (naxes[0] > AXMAX || naxes[1] > AXMAX || naxes[2] > AXMAX)
        std::cout <<
            "Error: current limit on axis size (" << AXMAX << ") exceeded!";

    npixels  = naxes[0] * naxes[1] * naxes[2];  // number of pixels in the image

    //std::cout << data << " readcube before data.resize" << std::endl;

    (*data).resize(npixels, 0.0);

    //std::cout << data << " readcube after data.resize" << std::endl;

    fpixel   = 1;
    nullval  = 0;    // don't check for null values in the image

    while (npixels > 0)
    {
      nbuffer = npixels;
      if (npixels > buffsize)
        nbuffer = buffsize;     /* read as many pixels as will fit in buffer */

      /* Note that even though the FITS images contains unsigned integer */
      /* pixel values (or more accurately, signed integer pixels with    */
      /* a bias of 32768),  this routine is reading the values into a    */
      /* float array.   Cfitsio automatically performs the datatype      */
      /* conversion in cases like this.                                  */

      if ( fits_read_img(fptr, TFLOAT, fpixel, nbuffer, &nullval,
                  buffer, &anynull, &status) )
           printerror( status );

      for (ii = 0; ii < nbuffer; ii++)  {
        (*data)[fpixel-1+ii] = buffer[ii];
      }

      npixels -= nbuffer;    /* increment remaining number of pixels */
      fpixel  += nbuffer;    /* next pixel to be read in image */
    }

    if ( fits_close_file(fptr, &status) )
         printerror( status );

    return;
}


/*
 * Write data in a DOUBLE array to FITS image as FLOAT.
 */
void save_image(fitsfile *outputfits, std::valarray<double> *data,
    long nelements) {

    // Copy DOUBLE results into FLOAT array and write to file.
    float *outputarray = (float *) malloc(nelements * sizeof(float));

    for (long ii=0; ii<nelements; ++ii) {
        outputarray[ii] = (float) (*data)[ii];
    }

    int status = 0;
    long fpixel = 1;

    if (fits_write_img(outputfits, TFLOAT, fpixel, nelements, outputarray,
        &status) )
        printerror( status );

    free(outputarray);
}


void cleanup_failed_output(fitsfile* outputfits, char* filename) {
    // Try to close file, then delete it
    int status = 0;
    fits_close_file(outputfits, &status);
    std::remove(filename);
}

