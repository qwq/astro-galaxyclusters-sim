/*
 * extract_cone_rbinned.cc
 *
 * Print radial profile along a given radial vector.
 */

#ifdef _MSC_VER
#include "MSconfig.h" // for truncation warning
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>
#include <fstream>
#include <fitsio.h>
#include <cmath>  // ceil, floor, ...
#include <valarray>
#include <algorithm>    // std::min_element, std::max_element
#include <omp.h>

#include "vector_math.h"
#include "progress.h"
#include "datacube.h"

#define PI 3.14159265
#define PROGRAM_VER "1.0"

#define NARGS 12
#define ARG_INFILE 1
#define ARG_C_X 2
#define ARG_C_Y 3
#define ARG_C_Z 4
#define ARG_N_X 5
#define ARG_N_Y 6
#define ARG_N_Z 7
#define ARG_R_MIN 8
#define ARG_R_MAX 9
#define ARG_THETA 10
#define ARG_RBIN 11


void print_usage();

int main(int argc, char* argv[])
{

    if (argc != NARGS)
    {
        print_usage();
        return 0;
    }

    char* inputfilename = argv[ARG_INFILE];

    double c[3] = {atof(argv[ARG_C_X]),atof(argv[ARG_C_Y]),atof(argv[ARG_C_Z])};
    double n[3] = {atof(argv[ARG_N_X]),atof(argv[ARG_N_Y]),atof(argv[ARG_N_Z])};
    double r_min = atof(argv[ARG_R_MIN]);
    double r_max = atof(argv[ARG_R_MAX]);
    double theta_max = atof(argv[ARG_THETA]);
    double r_bin = atof(argv[ARG_RBIN]);

    if (n[0]==0 && n[1]==0 && n[2]==0)
    {
        std::cout << "Normal vector length zero!" << std::endl;
        return 1;
    }

    if (r_max <= r_min || r_min < 0)
    {
        std::cout << "r_max > r_min > 0 required" << std::endl;
        return 1;
    }

    if (theta_max > 90)
    {
        std::cout << "theta_max < 90 degrees required";
        return 1;
    }

    if (r_bin > (r_max - r_min))
    {
        std::cout << "Need more than 1 bin.";
        return 1;
    }

    std::cout << "Command:" << std::endl;
    for (int i=0; i<argc; ++i) std::cout << argv[i] << " ";
    std::cout << std::endl;

    std::cout << "Center: " << c[0] << ", " << c[1] << ", " << c[2] << std::endl;

    /*
     * Normalize basis vectors
     */
    double nn=norm(n, 3);
    for (int i=0; i<3; ++i) n[i]/=nn;
    std::cout << "Unit normal: " << n[0] << ", " << n[1] << ", " << n[2] << std::endl;

    /*
     * Convert opening angle to a min. cosine value
     */
    double costheta_min = cos(theta_max*PI/180.0);


    /*************************
     * Read datacube file
     */
    Datacube datacube(inputfilename);
    if (!datacube.CheckInit()) {
        std::cout << "Data file was not read correctly!" << std::endl;
        return 1;
    }
    /*************************/

    std::valarray<float> contents = datacube.GetDataPtr();
    long* axes = new long[3];
    datacube.GetAxes(axes);
    long ax1, ax2, ax3;
    ax1 = axes[0];
    ax2 = axes[1];
    ax3 = axes[2];

    std::cout<<"Input data dimensions: "<<ax1<<" x "<<ax2<<" x "<<ax3<<std::endl;

    /*
     * Set up binned grid
     */
    long grid_n = (long) ceil((r_max-r_min)/r_bin);
    std::valarray<double> grid_sum(grid_n);
    std::valarray<double> grid_sumsq(grid_n);
    std::valarray<long> grid_count(grid_n);
    for (long i=0; i<grid_n; ++i)
    {
        grid_sum[i]=0.0;
        grid_sumsq[i]=0.0;
        grid_count[i]=0;
    }


    std::cout << "theta_max: " << theta_max*PI/180.0 << " radians" << std::endl;

    /*
     * Projection
     */
    std::cout << "Program version: " << PROGRAM_VER << std::endl;

    double t0 = omp_get_wtime();

    long counter = 0; // progress counter
    omp_set_num_threads(4);
    #pragma omp parallel for
    for (long z=0; z<ax3; ++z)
    {
        double d[3] = {0, 0, 0}; // displacement from center
        double l = 0; // length of displacement
        long inx, grid_inx;

        // temp holders private to each thread
        long pcounter;
        /*
        std::valarray<double> pgrid_sum(grid_n);
        std::valarray<double> pgrid_sumsq(grid_n);
        std::valarray<long> pgrid_count(grid_n);
        for (long i=0; i<grid_n; ++i)
        {
            pgrid_sum[i]=0.0;
            pgrid_sumsq[i]=0.0;
            pgrid_count[i]=0;
        }*/
        //

        for (long y=0; y<ax2; ++y)
        {
            for (long x=0; x<ax1; ++x)
            {
                inx = z*ax1*ax2 + y*ax1 + x;
                if (contents[inx] > 0)
                {
                    d[0] = x + 0.5 - c[0];
                    d[1] = y + 0.5 - c[1];
                    d[2] = z + 0.5 - c[2];
                    l = norm(d, 3);
                    if (r_min <= l && l <= r_max)
                    {
                        // Normalize d
                        for (int i=0; i<3; ++i) d[i] /= l;
                        // Is it in the opening angle?
                        if(dot(n, d) >= costheta_min)
                        {
                            grid_inx = (long) floor((l-r_min)/r_bin);

                            /*
                            pgrid_sum[grid_inx] += contents[inx];
                            pgrid_sumsq[grid_inx] += contents[inx]*contents[inx];
                            pgrid_count[grid_inx] += 1;
                            */

                            /////////////////////////////////////////
                            #pragma omp atomic update
                            grid_sum[grid_inx] += contents[inx];
                            #pragma omp atomic update
                            grid_sumsq[grid_inx] += contents[inx]*contents[inx];
                            #pragma omp atomic update
                            grid_count[grid_inx] += 1;
                            /////////////////////////////////////////

                        }
                    }
                }
            }
        }
        /*
        for (long i=0; i<grid_n; ++i)
        {
            #pragma omp atomic update
            grid_sum[i] += pgrid_sum[i];
            #pragma omp atomic update
            grid_sumsq[i] += pgrid_sumsq[i];
            #pragma omp atomic update
            grid_count[i] += pgrid_count[i];
        }
        */
        //////////////////////////////////
        #pragma omp atomic capture
        pcounter = ++counter;
        //////////////////////////////////
        if (pcounter%10==0) display_progress((float)pcounter/ax3);

        //#pragma omp critical
        //std::cout << buff.rdbuf();
        //++counter;
        //if (counter%10==0) display_progress((float)counter/ax3);
    }

    double t1 = omp_get_wtime();
    std::cout << t1-t0 << std::endl;

    // Compute the mean and stdev of binned data
    std::cout << "R0\tR1\tsum\tN\tmean\tstdev" << std::endl;
    std::cout << "***BEGIN DATA DUMP***" << std::endl;
    double grid_mean, grid_std;
    for (long i=0; i<grid_n; ++i)
    {
        grid_mean = grid_sum[i] / grid_count[i];
        grid_std = (grid_sumsq[i] / grid_count[i] - grid_mean*grid_mean)/(grid_count[i]-1);
        std::cout << r_min+i*r_bin << "\t" << r_min+(i+1)*r_bin << "\t" \
                << grid_sum[i] << "\t" << grid_count[i] << "\t" \
                << grid_mean << "\t" << grid_std << std::endl;
    }
    std::cout << "***END DATA DUMP***" << std::endl;

    return 0;
}

void print_usage() {
    std::cout << "extract_cone_rbinned (1 May 2019)" << std::endl;
    std::cout << "" << std::endl;
    std::cout << "Extract values from datacube along a given vector, in a cone within a given" << std::endl;
    std::cout << "opening angle. The profile is binned at a given interval." << std::endl;
    std::cout << "" << std::endl;
    std::cout << "Usage:" << std::endl;
    std::cout << "./extract_cone_rbinned infile cx cy cz nx ny nz r_min r_max theta rbin" << std::endl;
    std::cout << "" << std::endl;
    std::cout << "c \t (x, y, z) coordinates of the center" << std::endl;
    std::cout << "n \t (x, y, z) coordinates of the radial radial vector" << std::endl;
    std::cout << "r_min \t Lower limit of extraction radius" << std::endl;
    std::cout << "r_max \t Upper limit of extraction theta \tradius" << std::endl;
    std::cout << "theta \t Half-angle of cone" << std::endl;
    std::cout << "rbin \t Binning interval of radial profile" << std::endl;
}



