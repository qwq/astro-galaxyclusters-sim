/*
 * project_datacube
 *
 * Project a datacube given the direction of the viewer.
 *
 * A coordinate grid is first determined for the projected image. The datacube
 * is then looped over to deposit every pixel in the appropriate image bin.
 * Aliasing is handled by renormalizing the projection by the total pixel l.o.s.
 * distance through the datacube for every image pixel. This is analogous to
 * taking a zig-zagging sample of the l.o.s. integrand, and multiplying the unit
 * average by the column length. At worst, there is a data point for every
 * sqrt(3) px along l.o.s.. There is a reduction in spatial resolution of the
 * projected image when the datacube is viewed from such an angle that many
 * image pixels sample the l.o.s. in a very zig-zag way. A simple super-sampling
 * method is provided, by splitting each voxel cube into 8 and then drizzling
 * them onto bin=0.5 image pixels. This improves qualitatively the image
 * resolution when the projection is taken with an off-axis normal.
 *
 * To save time and disk space, one can specify a position in the datacube to
 * center at, and a radius out to which the image should include. This results
 * in a cropped square image.
 *
 * QW 20160422
 *
 * Revisions:
 * 20190426 - (2.0.3) Add image axes coordinates to header of output files.
 * 20180929 - (2.0.2) Add output file prefix option; ignore comment lines in text
 *            input. Add additional context info to output file header to describe
 *            the projection.
 * 20180925 - Add using text file as input to perform multiple projections from
 *            the same data file.
 * 20180824 - Switched from using CCFITS to CFITSIO library for FITS file I/O.
 */

#ifdef _MSC_VER
#include "MSconfig.h" // for truncation warning
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>
#include <fstream>
#include <fitsio.h>
#include <cmath>  // ceil, floor, ...
#include <valarray>
#include <algorithm>    // std::min_element, std::max_element
#include <cstring>  // std::strlen std::strcpy std::strtok
#include <string.h>  // strtok_r
#include <omp.h>

#include "datacube.h"
#include "vector_math.h"  // normalize_vector
#include "projection.h"

#define PROGRAM_VERSION "2.0.3"


void print_usage();
int parse_input(int argc, char* argv[]);
int process_input(int parc, char* pars[], Datacube* datacube);


int main(int argc, char* argv[])
{
    parse_input(argc, argv);

    return 0;
}


int parse_input(int argc, char* argv[]) {
    if (argc != 3 && argc != 4 && argc != 8 && argc != 12)
    {
        print_usage();
    }
    else {
        char* inputfilename = argv[1];  // Data file

        /*************************
         * Read datacube file
         */
        Datacube datacube(inputfilename);
        if (!datacube.CheckInit()) {
            std::cout << "Data file was not read correctly!" << std::endl;
            return 1;
        }
        /*************************/

        if (argc == 3 || argc == 4) {

            //////////////////////////////////////////
            // Output file name prefix option
            char* outprefix;
            int outprefix_len = 0; // Only used if argc == 4
            if (argc == 4) {
                // 4th arg is the batch text file
                outprefix = argv[3];
                outprefix_len = std::strlen(outprefix);
            }
            //////////////////////////////////////////

            // Open text file
            std::ifstream listfile(argv[2]);
            std::string input_line;

            char const *sep = " ";      // Separator for strtok

            int word_count, line_len;
            int line_num = 1;     // Index of first line
            char *word, *brkt;
            char *word_array[10]; // Longest expected number of args

            while (std::getline(listfile, input_line)) {
                // Parse content of each line into char* pars[]

                line_len = input_line.length();

                // Ignore empty or comment lines
                if (line_len == 0 || input_line[0] == '#') {
                    continue;
                }

                char input_line_c[line_len+1];  // Make copy as chararray
                std::strcpy(input_line_c, input_line.c_str());

                word_count = 0;
                for (word = strtok_r(input_line_c, sep, &brkt);
                     word;
                     word = strtok_r(NULL, sep, &brkt)) {
                    if (++word_count > 10) {  // Skips line when this reaches 11
                        break;
                    }

                    // Make copies of the word for word_array
                    char* buf;

                    //////////////////////////////////////////
                    // Output file name prefix option
                    if (argc == 4 && word_count == 1) {  // First word
                        buf = (char *) malloc(sizeof(char)*
                            (std::strlen(word) + outprefix_len + 1));
                        std::strcpy(buf, outprefix);
                        std::strcpy(buf+outprefix_len, word);
                    }
                    //////////////////////////////////////////

                    else {
                        buf = (char *) malloc(sizeof(char)*
                            (std::strlen(word) + 1));
                        std::strcpy(buf, word);
                    }

                    word_array[word_count-1] = buf;
                }
                if (word_count == 6 || word_count == 10) {
                    // call process_input(parc, pars);
                    process_input(word_count, word_array, &datacube);
                }
                else {
                    if (word_count > 0) { // Silently ignore empty lines
                        std::cout << "Bad input length (" << word_count <<
                            "), ignoring line " << line_num << ":" << std::endl;
                        std::cout << "\"" << input_line << "\"" << std::endl;
                    }
                }
                line_num++;
            }
        }
        else {
            process_input(argc-2, argv+2, &datacube);
        }
    }

    return 0;
}


int process_input(int parc, char* pars[], Datacube* datacube) {

    for (int i=0; i<parc; ++i) {
        std::cout << pars[i] << " ";
    }
    std::cout << std::endl;

    double t2 = omp_get_wtime();

    char* outputfilename = pars[0];

    double n[3] = {atof(pars[1]),atof(pars[2]),atof(pars[3])};
    double binning = atof(pars[4]);
    int subsample = atoi(pars[5]);

    if (n[0]==0 && n[1]==0 && n[2]==0) {
        std::cout << "Normal vector length zero!"  << std::endl;
        return 1;
    }

    if (binning != 0.5 && binning != 1 &&
        binning != 2 && binning != 3 && binning != 4) {
        std::cout << "Bin = " << binning <<
            " (allowed: 0.5, 1, 2, 3 or 4 only)" << std::endl;
        return 1;
    }

    if (subsample != 1 && subsample != 2 && subsample !=3 && subsample != 4) {
        std::cout << "Subsampling = " << subsample <<
            " (allowed: 1, 2, 3 or 4 only)" << std::endl;
        return 1;
    }

    if (parc == 10) {
        // 6, 7, 8 are center coords, 9 is radius
        if (atof(pars[6]) < 0.0 || atof(pars[7]) < 0.0 ||
            atof(pars[8]) < 0.0 || atof(pars[9]) < 0.0) {
            std::cout <<
                "Center coordinates and radius must be > 0!" << std::endl;
            return 1;
        }
    }


    /***********************************/
    /*
     * Try to create output file.
     */
    fitsfile *outputfits;
    int status = 0;
    if (fits_create_file(&outputfits, outputfilename, &status)) {
        printerror( status );
        return 1;
    }
    /**********************************/

    std::valarray<float> contents = datacube->GetDataPtr();
    long* axes = new long[3];
    datacube->GetAxes(axes);
    long ax1, ax2, ax3;
    ax1 = axes[0];
    ax2 = axes[1];
    ax3 = axes[2];

    /*
     * Calculate data cube boundaries
     */
    double cube_xmin, cube_xmax, cube_ymin, cube_ymax, cube_zmin, cube_zmax,
        cube_r = 0;
    bool crop_image = false;
    double cube_center[3];
    if (parc == 10) {
        cube_r = atof(pars[9]);
        cube_center[0] = atof(pars[6]);
        cube_center[1] = atof(pars[7]);
        cube_center[2] = atof(pars[8]);
        cube_xmin = std::max(0.0,     cube_center[0] - cube_r);
        cube_xmax = std::min(1.0*ax1, cube_center[0] + cube_r);
        cube_ymin = std::max(0.0,     cube_center[1] - cube_r);
        cube_ymax = std::min(1.0*ax2, cube_center[1] + cube_r);
        cube_zmin = std::max(0.0,     cube_center[2] - cube_r);
        cube_zmax = std::min(1.0*ax3, cube_center[2] + cube_r);

        crop_image = true;

        std::cout << "Cropped image centered at (" <<
            cube_center[0] << ", " <<
            cube_center[1] << ", " <<
            cube_center[2] << "), radius = " << cube_r << std::endl;
    }
    else {
        cube_xmin = 0.0;
        cube_xmax = 1.0*ax1;
        cube_ymin = 0.0;
        cube_ymax = 1.0*ax2;
        cube_zmin = 0.0;
        cube_zmax = 1.0*ax3;
    }


    /*
     * Normalize normal vector
     */
    normalize_vector(n, 3);
    std::cout << "Viewing from: " << n[0] << ", " << n[1] << ", " << n[2] <<
        std::endl;

    // Find image basis vectors
    double imax_x[3] = {0, 0, 0}; // x-axis of image plane
    double imax_y[3] = {0, 0, 0}; // y-axis of image plane
    calculate_image_basis_vectors(imax_x, imax_y, n);
    std::cout << "Image x-axis: " <<
        imax_x[0] << ", " << imax_x[1] << ", " << imax_x[2] << std::endl;
    std::cout << "Image y-axis: " <<
        imax_y[0] << ", " << imax_y[1] << ", " << imax_y[2] << std::endl;

    /*
     * Binning of the resulting image is done by scaling the basis vectors.
     * (When transforming back from image coordinates, need to multiply by
     * binning**2)
     */
    for (int i=0; i < 3; ++i) {
        imax_x[i] /= binning;
        imax_y[i] /= binning;
    }

    // Find image offset
    long imoff_x, imoff_y, imsize_x, imsize_y;
    calculate_image_offset(&imoff_x, &imoff_y, &imsize_x, &imsize_y,
        crop_image, binning, imax_x, imax_y, cube_center, cube_r,
        cube_xmin, cube_xmax, cube_ymin, cube_ymax, cube_zmin, cube_zmax);
    std::cout << "Image offset: (" <<
        imoff_x << ", " << imoff_y << ")" << std::endl;
    std::cout << "Image size: " <<
        imsize_x << " x " << imsize_y << std::endl;

    /*
     * Create output image HDU
     */
    long naxis    =   2;
    long naxes[2] = { imsize_x, imsize_y };
    long nelements = imsize_x*imsize_y;

    if ( fits_create_img(outputfits, FLOAT_IMG, naxis, naxes, &status) ) {
        printerror( status );
        cleanup_failed_output(outputfits, outputfilename);
        return 1;
    }
    //std::cout << "(2)\t" << omp_get_wtime() - t2 << std::endl;

    /*
     * Projection
     */
    std::valarray<double> projected_img((double) 0, nelements);
    std::valarray<long> projected_count((long) 0, nelements);
    std::valarray<double> projected_column((double) 0, nelements);


    //long ioff; // Datacube origin in image coordinates

    /*
     * Compute quantity to be drizzled
     */
    /*
    long sz, sy, sx;

    // data**2
    if (true) {
        for (int z=0; z<ax3; ++z) {
            sz = z*ax1*ax2;
            for (int y=0; y<ax2; ++y) {
                sy = sz + y*ax1;
                for (int x=0; x<ax1; ++x) {
                    sx = sy+x;
                    contents[sx] = contents[sx]*contents[sx];
                }
            }
        }
    }
    */

    /*
     * Dither loops
     *
     * For example, subsample = 1, dither_px = 1.0, and each pixel is drizzled
     * using their pixel centers (+0.5). For subsample = 2, each voxel is
     * split into 8 smaller ones; dither_px = 0.5, so each voxel is drizzled 8
     * times using permutations of +0.25, +0.75 as coordinates. For subsample
     * = 3, dither_px = 0.33, each pixel is drizzled 27 times using
     * permutations of +0.16, +0.5, +0.83 as coordinates.
     */
    double dither_x, dither_y, dither_z; // Dither px shifts
    double dither_px = 1.0/subsample; // Dither amount

    for (int di_z=0; di_z < subsample; ++di_z) {
        dither_z = dither_px * (0.5+di_z);
        for (int di_y=0; di_y < subsample; ++di_y) {
            dither_y = dither_px * (0.5+di_y);
            for (int di_x=0; di_x < subsample; ++di_x) {
                dither_x = dither_px * (0.5+di_x);
                //ioff = imoff_y*imsize_x + imoff_x;
                drizzle(&contents, &projected_img, &projected_count,
                    imax_x, imax_y, ax1, ax2, ax3, imsize_x, imsize_y,
                    imoff_x, imoff_y, dither_x, dither_y, dither_z, subsample);
            }
        }

    }
    std::cout << std::endl;

    /*
     * End dither loops
     */

    // Calculate column length for correct normalization
    calculate_column_length(imoff_x, imoff_y, imsize_x, imsize_y, ax1, ax2, ax3,
                            imax_x, imax_y, n, binning, &projected_column);

    //std::cout << "(5)\t" << omp_get_wtime() - t2 << std::endl;


    /*
     * Normalize the image by multiplying the mean of the spatial data
     * points sampled in the column by the length of the column.
     */
    for (long ii=0; ii<nelements; ++ii) {
        projected_img[ii] *= projected_column[ii]/projected_count[ii];
    }

    //std::cout << "(6)\t" << omp_get_wtime() - t2 << std::endl;

    save_image(outputfits, &projected_img, nelements);

    ////////////////////////////////////////////////////////////////////////////
    // Write keywords (must be after first creating extension header)
    if (fits_update_key(outputfits, TSTRING, "ORIGFILE", datacube->filename,
        "Origin data file", &status) ) {
        printerror( status );
        cleanup_failed_output(outputfits, outputfilename);
        return 1;
    }

    if (fits_update_key(outputfits, TSTRING, "PROJVER", (char*)PROGRAM_VERSION,
        "Projection program version", &status) ) {
        printerror( status );
        cleanup_failed_output(outputfits, outputfilename);
        return 1;
    }

    // Some metadata about the projection, helpful for recreating the same view
    // but say on a different data cube, or resolution, or crop, etc.
    /* This should be inherited from the datacube -- need to add metadata there
    if (fits_update_key(outputfits, TSTRING, "SNAPSHOT", context_snapshot_id,
        "Snapshot ID", &status) ) {
        printerror( status );
        cleanup_failed_output(outputfits, outputfilename);
        return 1;
    }*/

    if (fits_update_key(outputfits, TDOUBLE, "IMAXISXX", imax_x,
        "Image x-axis vector, x", &status) ) {
        printerror( status );
        cleanup_failed_output(outputfits, outputfilename);
        return 1;
    }

    if (fits_update_key(outputfits, TDOUBLE, "IMAXISXY", imax_x+1,
        "Image x-axis vector, y", &status) ) {
        printerror( status );
        cleanup_failed_output(outputfits, outputfilename);
        return 1;
    }

    if (fits_update_key(outputfits, TDOUBLE, "IMAXISXZ", imax_x+2,
        "Image x-axis vector, z", &status) ) {
        printerror( status );
        cleanup_failed_output(outputfits, outputfilename);
        return 1;
    }

    if (fits_update_key(outputfits, TDOUBLE, "IMAXISYX", imax_y,
        "Image y-axis vector, x", &status) ) {
        printerror( status );
        cleanup_failed_output(outputfits, outputfilename);
        return 1;
    }

    if (fits_update_key(outputfits, TDOUBLE, "IMAXISYY", imax_y+1,
        "Image y-axis vector, y", &status) ) {
        printerror( status );
        cleanup_failed_output(outputfits, outputfilename);
        return 1;
    }

    if (fits_update_key(outputfits, TDOUBLE, "IMAXISYZ", imax_y+2,
        "Image y-axis vector, z", &status) ) {
        printerror( status );
        cleanup_failed_output(outputfits, outputfilename);
        return 1;
    }

    if (fits_update_key(outputfits, TDOUBLE, "VIEWERX", n,
        "Viewer position, x", &status) ) {
        printerror( status );
        cleanup_failed_output(outputfits, outputfilename);
        return 1;
    }

    if (fits_update_key(outputfits, TDOUBLE, "VIEWERY", n+1,
        "Viewer position, y", &status) ) {
        printerror( status );
        cleanup_failed_output(outputfits, outputfilename);
        return 1;
    }

    if (fits_update_key(outputfits, TDOUBLE, "VIEWERZ", n+2,
        "Viewer position, z", &status) ) {
        printerror( status );
        cleanup_failed_output(outputfits, outputfilename);
        return 1;
    }

    if (fits_update_key(outputfits, TDOUBLE, "VIEWBIN", &binning,
        "Projection binning factor", &status) ) {
        printerror( status );
        cleanup_failed_output(outputfits, outputfilename);
        return 1;
    }

    if (fits_update_key(outputfits, TINT, "VIEWSUB", &subsample,
        "Projection subsample factor", &status) ) {
        printerror( status );
        cleanup_failed_output(outputfits, outputfilename);
        return 1;
    }

    if (fits_update_key(outputfits, TLOGICAL, "VIEWCROP", &crop_image,
        "Projection is cropped", &status) ) {
        printerror( status );
        cleanup_failed_output(outputfits, outputfilename);
        return 1;
    }

    if (crop_image) {
        if (fits_update_key(outputfits, TDOUBLE, "CENTERX", cube_center,
            "View center x", &status) ) {
            printerror( status );
            cleanup_failed_output(outputfits, outputfilename);
            return 1;
        }

        if (fits_update_key(outputfits, TDOUBLE, "CENTERY", cube_center+1,
            "View center y", &status) ) {
            printerror( status );
            cleanup_failed_output(outputfits, outputfilename);
            return 1;
        }

        if (fits_update_key(outputfits, TDOUBLE, "CENTERZ", cube_center+2,
            "View center z", &status) ) {
            printerror( status );
            cleanup_failed_output(outputfits, outputfilename);
            return 1;
        }

        if (fits_update_key(outputfits, TDOUBLE, "CROPRAD", &cube_r,
            "Crop radius", &status) ) {
            printerror( status );
            cleanup_failed_output(outputfits, outputfilename);
            return 1;
        }
    }
    ////////////////////////////////////////////////////////////////////////////

    if ( fits_close_file(outputfits, &status) ) {
        printerror( status );
        return 1;
    }


    /**************************************************************************/
    /*
     * Diagnostic images
     */

    // Same image dimensions as before
    //long naxis    =   2;
    //long naxes[2] = { imsize_x, imsize_y };
    //long nelements = imsize_x*imsize_y;
    /*
    // Counts image
    fitsfile *countfits;
    status = 0;
    if (fits_create_file(&countfits, "diagnostic_count.fits", &status))
        printerror( status );           // call printerror if error occurs
    if ( fits_create_img(countfits, FLOAT_IMG, naxis, naxes, &status) )
        printerror( status );

    save_image(countfits, &projected_count, nelements);

    if ( fits_close_file(countfits, &status) )
        printerror( status );

    // Column image
    fitsfile *columnfits;
    status = 0;
    if (fits_create_file(&columnfits, "diagnostic_column.fits", &status))
        printerror( status );           // call printerror if error occurs
    if ( fits_create_img(columnfits, FLOAT_IMG, naxis, naxes, &status) )
        printerror( status );

    save_image(columnfits, &projected_column, nelements);

    if ( fits_close_file(columnfits, &status) )
        printerror( status );
    */
    /**************************************************************************/


    std::cout << std::endl <<
        "Done! Saved output to " << outputfilename << std::endl;

    std::cout << "(7)\t" << omp_get_wtime() - t2 << std::endl;

    return 0;
}


void print_usage() {
    std::cout << "version " << PROGRAM_VERSION << std::endl << std::endl;
    std::cout << "Usage:" << std::endl;
    std::cout<<"./project_datacube in out nx ny nz bin sub"<<std::endl;
    std::cout << "\tor:" << std::endl;
    std::cout<<"./project_datacube in out nx ny nz bin sub cx cy cz r"<<std::endl;
    std::cout << "\tor:" << std::endl;
    std::cout<<"./project_datacube in outlist.txt"<<std::endl;
    std::cout << "\tor:" << std::endl;
    std::cout<<"./project_datacube in outlist.txt outputprefix"<<std::endl;
    std::cout<<std::endl;
    std::cout<<"\tin\tDatacube file"<<std::endl;
    std::cout<<"\tout\tOutput image file"<<std::endl;
    std::cout<<"\tnx"<<std::endl;
    std::cout<<"\tny\tNormal vector for projection (need not be normalized)"<<std::endl;
    std::cout<<"\tnz"<<std::endl;
    std::cout<<"\tbin\tImage binning (0.5, 1, 2, 3, 4)"<<std::endl;
    std::cout<<"\tsub\tSample datacube elements multiple times (integer 1 - 4)"<<std::endl;
    std::cout<<"\tcx"<<std::endl;
    std::cout<<"\tcy\t(Optional) Image center"<<std::endl;
    std::cout<<"\tcz"<<std::endl;
    std::cout<<"\tr\t(Optional) Radius to crop to"<<std::endl;
    std::cout<<"\toutlist.txt\tText file for batch projections, one per line."<<std::endl;
    std::cout<<"\t\tEither 6 (out nx ny nz bin sub) or " << std::endl;
    std::cout<<"\t\t10 (out nx ny nz bin sub cx cy cz r) arguments OK; " << std::endl;
    std::cout<<"\t\tbad input lines are skipped."<<std::endl;
}
