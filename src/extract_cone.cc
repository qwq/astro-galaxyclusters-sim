/*
 * extract_cone.cc
 *
 * Print data values within a specified cone, including their coordinates in a
 * 2D projection on the surface defined by the normal of the cone.
 */

#ifdef _MSC_VER
#include "MSconfig.h" // for truncation warning
#endif

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>
#include <fstream>
#include <fitsio.h>
#include <cmath>  // ceil, floor, ...
#include <valarray>
#include <algorithm>    // std::min_element, std::max_element
#include <omp.h>

#include "vector_math.h"
#include "progress.h"
#include "datacube.h"

#define PI 3.14159265
#define PROGRAM_VER "1.0"

void print_usage();

int main(int argc, char* argv[])
{

    if (argc != 14)
    {
        print_usage();
        return 0;
    }

    char* inputfilename = argv[1];

    double c[3] = {atof(argv[2]),atof(argv[3]),atof(argv[4])};
    double n[3] = {atof(argv[5]),atof(argv[6]),atof(argv[7])};
    double u[3] = {atof(argv[8]),atof(argv[9]),atof(argv[10])};
    double v[3] = {0, 0, 0};
    double r_min = atof(argv[11]);
    double r_max = atof(argv[12]);
    double theta_max = atof(argv[13]);

    if (n[0]==0 && n[1]==0 && n[2]==0) {
        std::cout << "Normal vector length zero!";
        return 1;
    }

    if (u[0]==0 && u[1]==0 && u[2]==0) {
        std::cout << "u vector length zero!";
        return 1;
    }

    if (r_max <= r_min or r_min < 0)
    {
        std::cout << "r_max > r_min > 0 required";
        return 1;
    }

    if (theta_max > 90)
    {
        std::cout << "theta_max < 90 degrees required";
        return 1;
    }

    std::cout << "Command:" << std::endl;
    for (int i=0; i<argc; ++i) std::cout << argv[i] << " ";
    std::cout << std::endl;

    std::cout << "Center: " << c[0] << ", " << c[1] << ", " << c[2] << std::endl;

    /*
     * Normalize basis vectors
     */
    double nn=norm(n, 3);
    for (int i=0; i<3; ++i) n[i]/=nn;
    std::cout << "Unit normal: " << n[0] << ", " << n[1] << ", " << n[2] << std::endl;

    double uu=norm(u, 3);
    for (int i=0; i<3; ++i) u[i]/=uu;
    std::cout << "u-basis vector: " << u[0] << ", " << u[1] << ", " << u[2] << std::endl;

    if (u[0]*n[0]+u[1]*n[1]+u[2]*n[2] == 1)
    {
        std::cout << "u and n cannot be parallel!";
        return 1;
    }

    /*
     * Convert opening angle to a min. cosine value
     */
    double costheta_min = cos(theta_max*PI/180.0);


    /*************************
     * Read datacube file
     */
    Datacube datacube(inputfilename);
    if (!datacube.CheckInit()) {
        std::cout << "Data file was not read correctly!" << std::endl;
        return 1;
    }
    /*************************/

    std::valarray<float> contents = datacube.GetDataPtr();
    long* axes = new long[3];
    datacube.GetAxes(axes);
    long ax1, ax2, ax3;
    ax1 = axes[0];
    ax2 = axes[1];
    ax3 = axes[2];

    std::cout<<"Input data dimensions: "<<ax1<<" x "<<ax2<<" x "<<ax3<<std::endl;

    /*
    Determine v-basis vector
    */
    cross(n,u,v); // u is not guaranteed to be perpendicular with normal, but now v is
    double v_norm = norm(v, 3);
    for (int i=0; i<3; ++i) v[i]/=v_norm; // Normalize v
    cross(v,n,u); // v and n are orthonormal so u is unit

    std::cout << "v-basis vector: " << v[0] << ", " << v[1] << ", " << v[2] << std::endl;
    std::cout << "theta_max: " << theta_max*PI/180.0 << " radians" << std::endl;

    /*
     * Projection
     */
    std::cout << "Program version: " << PROGRAM_VER << std::endl;
    std::cout << "***BEGIN DATA DUMP***" << std::endl;
    std::cout << "L\tphi\ttheta\tM\tU\tV" << std::endl;
    //omp_set_num_threads(4);
    //#pragma omp parallel for
    for (long z=0; z<ax3; ++z)
    {
        //std::stringstream buff;
        double d[3] = {0, 0, 0};  // displacement from center
        double l=0; // length of displacement
        double phi, theta, R, U, V;
        long inx;
        for (long y=0; y<ax2; ++y)
        {
            for (long x=0; x<ax1; ++x)
            {
                inx = z*ax1*ax2 + y*ax1 + x;
                if (contents[inx] > 0)
                {
                    d[0] = x + 0.5 - c[0];
                    d[1] = y + 0.5 - c[1];
                    d[2] = z + 0.5 - c[2];
                    l = norm(d, 3);
                    if (r_min <= l && l <= r_max)
                    {
                        // Normalize d
                        for (int i=0; i<3; ++i) d[i] /= l;
                        // Is it in the opening angle?
                        if(dot(n, d) >= costheta_min)
                        {
                            // Process some outputs
                            // distance from center, phi, theta, M
                            phi = atan2(dot(d, v),dot(d, u));
                            theta = acos(dot(d, n));
                            R = pow(2. - 2.*cos(theta), 0.5);
                            U = R*cos(phi);
                            V = R*sin(phi);
                            std::cout << l << "\t" << phi << "\t" << theta << "\t" << contents[inx] << "\t" << U << "\t" << V << std::endl;
                        }
                    }
                }
            }
        }
        //#pragma omp critical
        //std::cout << buff.rdbuf();
        //++counter;
        //if (counter%10==0) display_progress((float)counter/ax3);
    }
    std::cout << "***END DATA DUMP***" << std::endl;
    return 0;
}

void print_usage() {
    std::cout << "Usage:" << std::endl;
    std::cout<<"./project_datacube infile cx cy cz nx ny nz ux uy uz r_min r_max theta_max"<<std::endl;
    std::cout<<"c - center, n - normal, u - ra, dec = 0 axis"<<std::endl;
}
