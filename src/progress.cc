// progress.cc

/********************************************************
 * Display progress
 ********************************************************/

#include "progress.h"

/*
 * Display progress.
 */
void display_progress(float progress) {
    int barWidth = 70;

    if (progress > 1.0) progress = 1.0;

    std::cout << "[";
    int pos = barWidth * progress;
    for (int i = 0; i < barWidth; ++i) {
        if (i < pos) std::cout << "=";
        else if (i == pos) std::cout << ">";
        else std::cout << " ";
    }
    std::cout << "] " << int(progress * 100.0) << " %\r";
    std::cout.flush();
}
