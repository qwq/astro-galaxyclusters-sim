// projection_info.cc

/********************************************************
 * Print the image basis vectors and image offsets for a
 * particular set of inputs to projection_info.
 * Useful for applying the same transformation to
 * coordinates, for example.
 ********************************************************/

#include "projection.h"

#define PROGRAM_VERSION "2.0.2"

void print_usage();

int main(int argc, char* argv[]) {

    if (argc != 9 && argc != 13)
    {
        print_usage();
    }

    else {

        double n[3] = {atof(argv[4]),atof(argv[5]),atof(argv[6])};
        double binning = atof(argv[7]);
        int subsample = atoi(argv[8]);

        if (n[0]==0 && n[1]==0 && n[2]==0) {
            std::cout << "Normal vector length zero!" << std::endl;
            return 1;
        }
        if (binning != 0.5 && binning != 1 &&
            binning != 2 && binning != 3 && binning != 4) {
            std::cout << "Bin = " << binning <<
                " (allowed: 0.5, 1, 2, 3 or 4 only)" << std::endl;
            return 1;
        }
        if (subsample != 1 && subsample != 2 && subsample !=3 && subsample != 4) {
            std::cout << "Subsampling = " << subsample <<
                " (allowed: 1, 2, 3 or 4 only)" << std::endl;
            return 1;
        }
        if (argc == 13) {
            // 9, 10, 11 are center coords, 12 is radius
            if (atof(argv[9]) < 0.0 || atof(argv[10]) < 0.0 ||
                atof(argv[11]) < 0.0 || atof(argv[12]) < 0.0) {
                std::cout <<
                    "Center coordinates and radius must be > 0!" << std::endl;
                return 1;
            }
        }

        long ax1 = atoi(argv[1]), ax2 = atoi(argv[2]), ax3 = atoi(argv[3]);

        /*
         * Calculate data cube boundaries
         */
        double cube_xmin, cube_xmax, cube_ymin, cube_ymax, cube_zmin, cube_zmax,
            cube_r = 0;
        bool crop_image = false;
        double cube_center[3];
        if (argc == 13) {
            cube_r = atof(argv[12]);
            cube_center[0] = atof(argv[9]);
            cube_center[1] = atof(argv[10]);
            cube_center[2] = atof(argv[11]);
            cube_xmin = std::max(0.0,     cube_center[0] - cube_r);
            cube_xmax = std::min(1.0*ax1, cube_center[0] + cube_r);
            cube_ymin = std::max(0.0,     cube_center[1] - cube_r);
            cube_ymax = std::min(1.0*ax2, cube_center[1] + cube_r);
            cube_zmin = std::max(0.0,     cube_center[2] - cube_r);
            cube_zmax = std::min(1.0*ax3, cube_center[2] + cube_r);

            crop_image = true;

            std::cout << "Cropped image centered at (" <<
                cube_center[0] << ", " <<
                cube_center[1] << ", " <<
                cube_center[2] << "), radius = " << cube_r << std::endl;
        }
        else {
            cube_xmin = 0.0;
            cube_xmax = 1.0*ax1;
            cube_ymin = 0.0;
            cube_ymax = 1.0*ax2;
            cube_zmin = 0.0;
            cube_zmax = 1.0*ax3;
        }

        /*
         * Normalize normal vector
         */
        normalize_vector(n, 3);
        std::cout << "Viewing from: " << n[0] << ", " << n[1] << ", " << n[2] <<
            std::endl;

        // Find image basis vectors
        double imax_x[3] = {0, 0, 0}; // x-axis of image plane
        double imax_y[3] = {0, 0, 0}; // y-axis of image plane
        calculate_image_basis_vectors(imax_x, imax_y, n);
        std::cout << "Image x-axis: " <<
            imax_x[0] << ", " << imax_x[1] << ", " << imax_x[2] << std::endl;
        std::cout << "Image y-axis: " <<
            imax_y[0] << ", " << imax_y[1] << ", " << imax_y[2] << std::endl;

        /*
         * Binning of the resulting image is done by scaling the basis vectors.
         * (When transforming back from image coordinates, need to multiply by
         * binning**2)
         */
        for (int i=0; i < 3; ++i) {
            imax_x[i] /= binning;
            imax_y[i] /= binning;
        }

        // Find image offset
        long imoff_x, imoff_y, imsize_x, imsize_y;
        calculate_image_offset(&imoff_x, &imoff_y, &imsize_x, &imsize_y,
            crop_image, binning, imax_x, imax_y, cube_center, cube_r,
            cube_xmin, cube_xmax, cube_ymin, cube_ymax, cube_zmin, cube_zmax);
        std::cout << "Image offset: (" <<
            imoff_x << ", " << imoff_y << ")" << std::endl;
        std::cout << "Image size: " <<
            imsize_x << " x " << imsize_y << std::endl;
    }
    return 0;

}


void print_usage() {
    std::cout << "version " << PROGRAM_VERSION << std::endl << std::endl;
    std::cout << "Usage:" << std::endl;
    std::cout<<"./projection_info lx ly lz nx ny nz bin sub"<<std::endl;
    std::cout << "\tor:" << std::endl;
    std::cout<<"./projection_info lx ly lz nx ny nz bin sub cx cy cz r"<<std::endl;
    std::cout<<std::endl;
    std::cout<<"\tlx"<<std::endl;
    std::cout<<"\tly\tDimensions of datacube"<<std::endl;
    std::cout<<"\tlz"<<std::endl;
    std::cout<<"\tnx"<<std::endl;
    std::cout<<"\tny\tNormal vector for projection (need not be normalized)"<<std::endl;
    std::cout<<"\tnz"<<std::endl;
    std::cout<<"\tbin\tImage binning (0.5, 1, 2, 3, 4)"<<std::endl;
    std::cout<<"\tsub\tSample datacube elements multiple times (integer 1 - 4)"<<std::endl;
    std::cout<<"\tcx"<<std::endl;
    std::cout<<"\tcy\t(Optional) Image center"<<std::endl;
    std::cout<<"\tcz"<<std::endl;
    std::cout<<"\tr\t(Optional) Radius to crop to"<<std::endl;
}
