// vector_math.cc

/********************************************************
 * Vector calculations.
 ********************************************************/

#include "vector_math.h"

/*
 * Calculate dot product.
 */
double dot(double a[], double b[]) {
    return a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
}


/*
 * Calculate cross-product.
 */
void cross(double a[], double b[], double out[]) {
    out[0]=a[1]*b[2]-a[2]*b[1];
    out[1]=a[2]*b[0]-a[0]*b[2];
    out[2]=a[0]*b[1]-a[1]*b[0];
}


/*
 * Calculate the vector norm.
 */
double norm(double *a, int dim) {
    double cs = 0;
    for (int i=0; i < dim; ++i) {
        cs += a[i]*a[i];
    }
    return pow(cs, 0.5);
}


/*
 * Normalize a vector in-place.
 */
void normalize_vector(double *v, int dim) {
    double vn = norm(v, dim);
    for (int i=0; i < dim; ++i) {
        v[i] /= vn;
    }
}
