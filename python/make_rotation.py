"""
Make a batch of images in sequence of rotation angles about an axis.

Start with a format string for the input to project_datacube, then generate a
batch text file. Use the same batch text file for different datacubes (using
different output prefixes with project_datacube).

>>> format_string='{name}.fits {x} {y} {z} 2 1'
>>> txt = generate_batchfile(250, format_string)
271 points generated.
>>> with open('271pt_bin2.txt', 'w') as fh:
>>>     fh.write(txt)

Then,

./project_datacube 390_rho.fits 271pt_bin2.txt 390_rho_
./project_datacube 395_rho.fits 271pt_bin2.txt 395_rho_
...

"""

import numpy as np


def name_string(label):
    """
    Label to use encoding the ra and dec info in the output file name.
    """
    return '{phi:.2f}_{theta:.2f}'.format(phi=label[0], theta=label[1])


def coord_string(val):
    """
    Format the coordinate string (truncate it).
    """
    return '{:.8e}'.format(val)


def nearest_odd(val):
    """
    Round to the nearest odd integer.
    """
    if np.floor(val) % 2 == 0:
        return int(np.ceil(val))
    return int(np.floor(val))


def transform(polar):
    """
    Calculate coordinate transformation from polar to cartesian.
    """
    sintheta = np.sin(polar[1])
    sinphi = np.sin(polar[0])
    costheta = np.cos(polar[1])
    cosphi = np.cos(polar[0])
    return [sintheta * cosphi, sintheta * sinphi, costheta]


def rad_to_deg(rad):
    """
    Convert radians to degrees.
    """
    return 180. / np.pi * rad


def equidist_sphere(points):
    """
    Distribute a specified number of points on the surface of a sphere so that
    their spacing is as equal as possible. They will be placed on discrete
    lines of latitude but will not align along longitudinal lines. Coordinate
    system: spherical polar, in radians.

    The total number of points that can be allocated in the end is not
    guaranteed.

    For quick reference, see Markus Deserno, "How to generate equidistributed
    points on the surface of a sphere", 2004-09-28.
    https://www.cmu.edu/biolphys/deserno/pdf/sphere_equi.pdf
    """
    coords = []  # polar coordinates of points
    mean_area = 4 * np.pi / points
    scale = mean_area**0.5
    n_theta = int(np.round(np.pi / scale))  # number of latitudes (theta)
    dtheta = np.pi / n_theta
    dphi = mean_area / dtheta
    for i_theta in range(n_theta):
        theta = np.pi * (i_theta + 0.5) / n_theta
        n_phi = int(np.round(2 * np.pi * np.sin(theta) / dphi))
        # number of longitudes (phi)
        for i_phi in range(n_phi):
            phi = 2 * np.pi * i_phi / n_phi
            coords.append([phi, theta])
    return coords


def equidist_hemisphere(points, verbose=False, offset=0.5, lim=0):
    """
    Only use the northern hemisphere (theta from 0 to pi/2).

    There is a slight variation from the spherical version stemming from the
    desire to have points on the equator. This is guaranteed if total number
    of latitudes is odd. So the calculation begins for the full sphere, but
    constrain total number of latitudes to be odd; then, only the northern
    hemisphere + equator are considered.

    The total number of points that can be allocated in the end is not
    guaranteed. Because the full equator is included (most distributions will
    not place points on opposing sides of the origin), there will always be
    slightly more points than requested.

    Output list of coordinates as (phi, theta) pairs.

    By default, points are placed in the center of bands of latitudes.
    (offset = 0.5)

    The last latitude band is the equatorial one (lim = 0). To stop just
    before, set lim = -1.
    """

    coords = []  # polar coordinates of points
    mean_area = 2 * np.pi / points
    scale = mean_area**0.5
    n_theta = nearest_odd(np.pi / scale)  # number of latitudes (theta)
    dtheta = np.pi / n_theta
    dphi = mean_area / dtheta
    if verbose:
        print('n_theta = %d' % n_theta)
    for i_theta in range(n_theta // 2 + 1 + lim):
        theta = np.pi * (i_theta + offset) / n_theta
        n_phi = int(np.round(2 * np.pi * np.sin(theta) / dphi))
        # number of longitudes (phi)
        for i_phi in range(n_phi):
            phi = 2 * np.pi * i_phi / n_phi
            coords.append([phi, theta])
    if verbose:
        print('%d points generated.' % len(coords))
    return coords


def polar_to_cartesian(coords):
    """
    Transform a list of polar coordinates (in radians) to a list of cartesian
    coordinates.
    """
    return [transform(_) for _ in coords]


def polar_to_degree(coords):
    """
    Transform a list of polar coordinates (in radians) to a list of polar
    coordinates (in degrees).
    """
    return [[rad_to_deg(_[0]), rad_to_deg(_[1])] for _ in coords]


def generate_batchfile(points, format_string, distfunc=equidist_hemisphere):
    """
    Generate a text file as input to project_datacube.

    A format_string for the command must be supplied, using the keywords:

    name - an identifying name for the output file containing the ra and dec
        of the projection.
    x, y, z - the cartesian coordinates of the viewing direction.

    Example:

    format_string = '{name}.fits {x} {y} {z} 1 1'
    txt = generate_batchfile(5, format_string)
    print(txt)

    This function truncates the floating point values when forming the strings
    to be used for input.
    """

    points_set = distfunc(points)
    labels = polar_to_degree(points_set)
    coords = polar_to_cartesian(points_set)
    content = ''
    for (vector, label) in zip(coords, labels):
        content += format_string.format(
            name=name_string(label),
            x=coord_string(vector[0]),
            y=coord_string(vector[1]),
            z=coord_string(vector[2])) + '\n'

    print('%d points generated.' % len(coords))
    return content


def combine_views(points, format_string, outfilename):
    """
    Merge projections into a single FITS file with multiple extensions.

    Use the same input as generate_batchfile so that the ra and dec labels are
    identical, thus the files can be correctly located.

    Example:
    >>> fmt_string = '405_0840.{name}.fits'
    >>> make_rotation.combine_views(250, fmt_string, '405_0840_views.fits')
    """
    import astropy.io.fits as pf
    import os

    if os.path.exists(outfilename):
        raise FileExistsError

    points_set = equidist_hemisphere(points)
    labels = polar_to_degree(points_set)

    # Check all files exist
    block = False
    for label in labels:
        filename = format_string.format(name=name_string(label))
        if not os.path.isfile(filename):
            print('Error: %s does not exist!' % filename)
            block = True

    if block:
        return False

    output = pf.HDUList([pf.PrimaryHDU()])
    for label in labels:
        fitsfile = pf.open(format_string.format(name=name_string(label)))
        output.append(fitsfile[0].copy())
        fitsfile.close()

    output.writeto(outfilename)

    return True
