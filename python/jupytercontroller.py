"""
Generate controls for viewing in DS9, by means of ipywidgets in Jupyter
notebook.
"""

from ipywidgets import interact  # interact_manual, interactive, fixed,
import ipywidgets as widgets
from ipywidgets import IntSlider, Layout
import ds9actions
import ds9utils
from viewmanager import parse_projection_info


class JupyterController:
    """
    Controller making use of ipywidgets in jupyter notebook, to use in tandem
    with ViewManager for a more interactive viewing.
    """

    def __init__(self):
        self.vm_T = None
        self.vm_X = None
        self.control_snapshot = None
        self.control_view = None
        self.control_ra = None
        self.control_dec = None

    def check_vm(self):
        """
        Check for link with DS9 view object.
        """
        if self.vm_T is None or self.vm_X is None:
            print('Need to link with DS9 view manager first.')
            return False

        return True

    def link_view(self, vm_T, vm_X):
        """
        Set link with DS9 view object.
        """
        self.vm_T = vm_T
        self.vm_X = vm_X
        self._configure_widgets()  # Widgets may depend on data

    def _configure_widgets(self):
        """
        Configure ipywidgets.
        """
        self.control_snapshot = widgets.IntSlider(
            min=380,
            max=495,
            step=5,
            readout=False,
            layout=Layout(width='450px')
        )

        self.control_ra = IntSlider(
            description='Azimuth',
            min=0,
            max=359,
            step=1,
            readout=True,
            layout=Layout(width='500px')
        )

        self.control_dec = IntSlider(
            description='Polar',
            min=0,
            max=90,
            step=1,
            readout=True,
            layout=Layout(width='500px')
        )

    def place_time_slider(self):
        """
        Place an ipywidgets integer slider for scrolling through snapshots,
        for a given viewing position.

        The temperature and X-ray cubes have the same projection info so
        header data from either can be used for this.
        """
        if not self.check_vm():
            return

        dsu = ds9utils.DS9Utils(self.vm_T.ds9, '../meta/gc_peak.txt')

        @interact(inx=self.control_snapshot,
                  view=self.vm_T.space_slices)
        def display_slice(inx, view='0.00_23.68'):
            slice_index = 1 + (inx - 380) // 5

            dsu.frame_goto(1)
            dsu.region_delete_all()
            self.vm_T.show_view(view)
            dsu.frame_goto(2)
            dsu.region_delete_all()
            self.vm_X.show_view(view)

            dsu.cube_goto(slice_index)
            dsu.lock_frame('image')
            dsu.lock_slice('image')

            slice_hdr = self.vm_T.get_header_view(slice_index)

            # Draw a 2 Mpc (comoving) circle centered on GC X-ray center
            projection_pars = parse_projection_info(slice_hdr)
            dsu.frame_goto(1)
            x, y = dsu.draw_gc_circle(
                slice_index - 1, 2000 / (7.4 / 0.7), projection_pars)
            dsu.zoom_pan_to(x, y)
            dsu.frame_goto(2)
            dsu.draw_gc_circle(slice_index - 1, 2000 /
                               (7.4 / 0.7), projection_pars)

            # Print stuff for quick copy & paste
            print('snapshot_id = %d' % inx)
            print('n = [%f, %f, %f]' % (
                slice_hdr['VIEWERX'],
                slice_hdr['VIEWERY'],
                slice_hdr['VIEWERZ']
            ))

    def place_space_slider(self):
        """
        Place ipywidgets integer sliders for scrolling through azimuthal angle
        and declination angle, for a given snapshot.
        """
        if not self.check_vm():
            return

        dsu = ds9utils.DS9Utils(self.vm_T.ds9, '../meta/gc_peak.txt')

        lookup = build_point_lookup(self.vm_T.space_slices)
        decs = list(lookup.keys())

        current_snapshot = None
        current_slice = None
        current_hdr = None

        @interact(alt=self.control_dec, az=self.control_ra,
                  snapshot_id=[str(_) for _ in self.vm_T.time_slices])
        def display_altaz(alt, az, snapshot_id='440'):
            nonlocal current_slice, current_snapshot, current_hdr

            snapshot_index = 1 + (int(snapshot_id) - 380) // 5

            # Convert alt, az angles to closest cached image
            dec = decs[nearest(decs, alt)]
            ras = lookup[dec]
            ra = ras[nearest(ras, az)]

            slice_index = 1 + \
                self.vm_T.space_slices.index('%.2f_%.2f' % (ra, dec))

            if not (current_snapshot == snapshot_index and
                    current_slice == slice_index):
                current_snapshot = snapshot_index
                current_slice = slice_index

                dsu.frame_goto(1)
                dsu.region_delete_all()
                self.vm_T.show_snapshot(int(snapshot_id))
                dsu.frame_goto(2)
                dsu.region_delete_all()
                self.vm_X.show_snapshot(int(snapshot_id))

                dsu.cube_goto(slice_index)
                dsu.lock_frame('image')
                dsu.lock_slice('image')

                current_hdr = self.vm_T.get_header_snapshot(slice_index)

                # Draw a 2 Mpc (comoving) circle centered on GC X-ray center
                projection_pars = parse_projection_info(current_hdr)
                dsu.frame_goto(1)
                x, y = dsu.draw_gc_circle(
                    snapshot_index - 1, 2000 / (7.4 / 0.7), projection_pars)
                dsu.zoom_pan_to(x, y)
                dsu.frame_goto(2)
                dsu.draw_gc_circle(snapshot_index - 1, 2000 /
                                   (7.4 / 0.7), projection_pars)

            print('label = \'%.2f_%.2f\'' % (ra, dec))
            print('n = [%f, %f, %f]' % (
                current_hdr['VIEWERX'],
                current_hdr['VIEWERY'],
                current_hdr['VIEWERZ']
            ))


def nearest(arr, val):
    """
    arr is a sorted array. return the index of the nearest value to val
    """
    if arr == []:
        print('Input array is empty!')
        return False

    if len(arr) == 1:
        return 0

    i, j = 0, len(arr) - 1
    while i < j - 1:
        m = (i + j) // 2
        if arr[m] >= val:
            j = m
        elif arr[m] <= val:
            i = m
    if val - arr[i] < arr[j] - val:
        return i
    else:
        return j


def build_point_lookup(space_slices):
    """
    Build the list of viewing angles present in the dataset.
    """
    index = {}
    for _ in space_slices:
        coords = _.split('_')
        ra = float(coords[0])
        dec = float(coords[1])
        if dec in index:
            index[dec].append(ra)
        else:
            index[dec] = [ra]
    return index
