"""
Transformations for projection.
"""

import numpy as np


def cube_to_image(axes, coords, norm, binning=1, crop=False,
                  cube_center=[0, 0, 0], cube_r=0, verbose=False):
    """
    Transform vector from (x, y, z) in datacube to (x, y) in image
    coordinates.

    Note -- both are 0-based indices; in numpy array, the 0th dimension is the
    z-axis; data is contiguous along x, then y, then z.

    The applied transformation must be consistent with the formula applied by
    drizzle() in projection.cc.
    """
    coords = np.float64(coords)
    n = vector_normalize(norm)
    (imax_x, imax_y) = image_basis_vectors(norm)
    (imoff_x, imoff_y, imsize_x, imsize_y) = image_offset(
        axes, norm, crop, binning, cube_center, cube_r)
    if verbose:
        print('Viewing from: %f, %f, %f' % (n[0], n[1], n[2]))
        print('Image x-axis: %f, %f, %f' % (imax_x[0], imax_x[1], imax_x[2]))
        print('Image y-axis: %f, %f, %f' % (imax_y[0], imax_y[1], imax_y[2]))
        print('Image offset: (%d, %d)' % (imoff_x, imoff_y))
        print('Image size: %d x %d' % (imsize_x, imsize_y))

    return (vector_dot(coords, imax_x) - imoff_x,
            vector_dot(coords, imax_y) - imoff_y)


def image_basis_vectors(n):
    """
    Calculate image basis vectors based on norm vector.
    """
    n = vector_normalize(n)
    imax_x = np.zeros(3, dtype=np.float64)
    imax_y = np.zeros(3, dtype=np.float64)
    if n[0] == 0 and n[1] == 0:  # n parallel to z-axis
        imax_x[0] = 1
        if n[2] > 0:
            imax_y[1] = 1
        else:
            imax_y[1] = -1
    else:
        imax_y[2] = 1
        imax_x = vector_normalize(vector_cross(imax_y, n))
        imax_y = vector_cross(n, imax_x)
    return imax_x, imax_y


def image_offset(axes, norm, crop_image=False, binning=1,
    cube_center=[0, 0, 0], cube_r=0, verbose=False):
    """
    Compute the offset (in image coordinates) to adjust projected values with,
    so that the projection volume fits within the image. Use this to translate
    datacube coordinates to image coordinates, for a particular set of
    projection parameters.
    """
    imax_x, imax_y = image_basis_vectors(norm)

    if not crop_image is True:
        cubexmin = 0.
        cubexmax = 1. * axes[0]
        cubeymin = 0.
        cubeymax = 1. * axes[1]
        cubezmin = 0.
        cubezmax = 1. * axes[2]

        c1 = np.float64([cubexmin, cubeymin, cubezmin])
        c2 = np.float64([cubexmax, cubeymin, cubezmin])
        c3 = np.float64([cubexmin, cubeymax, cubezmin])
        c4 = np.float64([cubexmin, cubeymin, cubezmax])
        c5 = np.float64([cubexmax, cubeymax, cubezmin])
        c6 = np.float64([cubexmax, cubeymin, cubezmax])
        c7 = np.float64([cubexmin, cubeymax, cubezmax])
        c8 = np.float64([cubexmax, cubeymax, cubezmax])

        corner_x = np.float64([
            vector_dot(imax_x, c1), vector_dot(imax_x, c2),
            vector_dot(imax_x, c3), vector_dot(imax_x, c4),
            vector_dot(imax_x, c5), vector_dot(imax_x, c6),
            vector_dot(imax_x, c7), vector_dot(imax_x, c8)
        ])

        corner_y = np.float64([
            vector_dot(imax_y, c1), vector_dot(imax_y, c2),
            vector_dot(imax_y, c3), vector_dot(imax_y, c4),
            vector_dot(imax_y, c5), vector_dot(imax_y, c6),
            vector_dot(imax_y, c7), vector_dot(imax_y, c8)
        ])

        imoff_x = int(np.floor(np.min(corner_x)))
        imsize_x = int(np.ceil(np.max(corner_x)) - imoff_x)
        imoff_y = int(np.floor(np.min(corner_y)))
        imsize_y = int(np.ceil(np.max(corner_y)) - imoff_y)

    else:
        cubexmin = max(0.0, cube_center[0] - cube_r)
        cubexmax = min(1. * axes[0], cube_center[0] + cube_r)
        cubeymin = max(0.0, cube_center[1] - cube_r)
        cubeymax = min(1. * axes[1], cube_center[1] + cube_r)
        cubezmin = max(0.0, cube_center[2] - cube_r)
        cubezmax = min(1. * axes[2], cube_center[2] + cube_r)

        imoff_x = int(vector_dot(imax_x, cube_center) - cube_r / binning)
        imsize_x = int(np.ceil(2 * cube_r / binning))
        imoff_y = int(vector_dot(imax_y, cube_center) - cube_r / binning)
        imsize_y = imsize_x

        if verbose:
            print(imsize_x, imsize_y)

    return imoff_x, imoff_y, imsize_x, imsize_y


def vector_len(a):
    """
    Compute the length of a vector.
    """
    return np.sum(np.float64(a)**2)**.5


def vector_dot(a, b):
    """
    Compute the dot product of two vectors.
    """
    if len(a) != len(b):
        print('Input vectors have different dimensions!')
        raise ValueError
    return np.sum(np.float64(a) * np.float64(b))


def vector_cross(a, b):
    """
    Compute the cross product of two vectors.
    """
    if len(a) != 3 or len(b) != 3:
        raise ValueError

    return np.float64([
        a[1] * b[2] - a[2] * b[1],
        a[2] * b[0] - a[0] * b[2],
        a[0] * b[1] - a[1] * b[0]])


def vector_normalize(a):
    """
    Normalize a vector by dividing by its length.
    """
    return np.float64(a) / vector_len(a)
