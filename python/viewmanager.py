"""
Fetch data from various snapshot/view images and display them as slices in a
datacube in DS9.
"""

import os
import pyds9
import astropy.io.fits as pf
import numpy as np
import ds9actions


def parse_projection_info(hdr):
    """
    Get projection parameters from header.
    """
    pars = {}
    try:
        pars['n'] = [hdr['VIEWERX'], hdr['VIEWERY'], hdr['VIEWERZ']]
        pars['binning'] = hdr['VIEWBIN']
        pars['crop'] = hdr['VIEWCROP']
        pars['cube_center'] = [hdr['CENTERX'], hdr['CENTERY'], hdr['CENTERZ']]
        pars['cube_r'] = hdr['CROPRAD']
    except IndexError:
        pass
    return pars


class ViewManager:
    """
    Manage caching data and sending data to DS9 for display.
    """

    def __init__(self, time_slices, space_slices, xpa_method=None):
        if xpa_method is None:
            self.ds9 = pyds9.DS9()
            print('Starting new DS9 instance...')
        else:
            self.ds9 = pyds9.DS9(xpa_method)
            print('Connected to DS9 at %s' % xpa_method)
        self.time_slices = time_slices
        self.space_slices = space_slices
        self._cache = {}
        self._cache_view = None
        self._cache_view_label = None
        self._cache_view_fits = None
        self._cache_view_fits_label = None
        self._cache_snapshot = None
        self._cache_snapshot_id = None
        self._cache_snapshot_fits = None
        self._cache_snapshot_fits_id = None
        self._datadir = ''
        self._datasuffix = ''  # e.g. {snapshot}{suffix}.{view}.fits

    def _check_views_exist(self, name_string):
        """
        Check that all views exist. name_string has snapshot ID info embedded.
        """
        block = False
        for label in self.space_slices:
            filename = name_string.format(label=label)
            if not os.path.isfile(filename):
                print('Error: %s does not exist!' % filename)
                block = True
        return not block

    def _check_snapshots_exist(self, name_string):
        """
        Check that all snapshots exist. name_string has view info embedded.
        """
        block = False
        for snap in self.time_slices:
            filename = name_string.format(id=snap)
            if not os.path.isfile(filename):
                print('Error: %s does not exist!' % filename)
                block = True
        return not block

    def set_dir(self, loc, suffix):
        """
        Set data file directory.
        """
        loc = loc.rstrip('/')
        if os.path.isdir(loc):
            self._datadir = loc
        else:
            print('%s is not a valid directory!' % loc)
        self._datasuffix = suffix

    def cache_snapshot(self, snapshot_id):
        """
        Store a multiple slices cube (z=viewing angle) for a single view.
        """
        if self._cache_snapshot_id == snapshot_id:
            return self._cache_snapshot

        name_string = '%s/%d%s.{label}.fits' % (
            self._datadir, snapshot_id, self._datasuffix)
        self._check_views_exist(name_string)

        holder = np.zeros(
            (len(self.space_slices), 1024, 1024), dtype=np.float32)
        for (i, label) in enumerate(self.space_slices):
            tmpfile = pf.open(name_string.format(label=label))
            holder[i, :, :] = tmpfile[0].data * 1.0
            tmpfile.close()

        holder.reshape((len(self.space_slices), 1024, 1024))
        self._cache_snapshot = holder
        self._cache_snapshot_id = snapshot_id

        return self._cache_snapshot

    def cache_snapshot_fits(self, snapshot_id):
        """
        Store a multiple slices cube (z=viewing angle) for a single view, as
        FITS HDU list.
        """
        if self._cache_snapshot_fits_id == snapshot_id:
            return self._cache_snapshot_fits

        name_string = '%s/%d%s.{label}.fits' % (
            self._datadir, snapshot_id, self._datasuffix)
        self._check_views_exist(name_string)

        # Create HDUList object
        holder = pf.HDUList([pf.PrimaryHDU()])
        for label in self.space_slices:
            tmpfile = pf.open(name_string.format(label=label))
            holder.append(tmpfile[0].copy())
            tmpfile.close()

        self._cache_snapshot_fits = holder
        self._cache_snapshot_fits_id = snapshot_id

        return holder

    def cache_view(self, view_name):
        """
        Store a multiple slices cube (z=snapshot id) for a single view.
        """
        if self._cache_view_label == view_name:
            return self._cache_view

        name_string = '%s/{id}%s.%s.fits' % (
            self._datadir, self._datasuffix, view_name)
        self._check_snapshots_exist(name_string)

        holder = np.zeros(
            (len(self.time_slices), 1024, 1024), dtype=np.float32)
        for (i, snap) in enumerate(self.time_slices):
            tmpfile = pf.open(name_string.format(id=snap))
            holder[i, :, :] = tmpfile[0].data * 1.0
            tmpfile.close()

        holder.reshape((len(self.time_slices), 1024, 1024))

        self._cache_view = holder
        self._cache_view_label = view_name

        return self._cache_view

    def cache_view_fits(self, view_name):
        """
        Store a multiple slices cube (z=snapshot id) for a single view, as
        FITS HDUList. This keeps FITS header accessible in each HDU.
        """
        if self._cache_view_fits_label == view_name:
            return self._cache_view_fits

        name_string = '%s/{id}%s.%s.fits' % (
            self._datadir, self._datasuffix, view_name)
        self._check_snapshots_exist(name_string)

        # Create HDUList object
        holder = pf.HDUList([pf.PrimaryHDU()])
        for snap in self.time_slices:
            tmpfile = pf.open(name_string.format(id=snap))
            holder.append(tmpfile[0].copy())
            tmpfile.close()

        self._cache_view_fits = holder
        self._cache_view_fits_label = view_name

        return holder

    def show_view(self, view_name):
        """
        Load a multiple slices cube in DS9 (z=snapshot id) for a single view.
        """
        if self._cache_view_fits_label != view_name:
            dsa = ds9actions.DS9Actions(self.ds9)
            dsa.file_openas_mecube(self.cache_view_fits(view_name))
        return True

    def show_snapshot(self, snapshot_id):
        """
        Load snapshot_id in DS9 (if not already loaded).
        """
        if self._cache_snapshot_fits_id != snapshot_id:
            dsa = ds9actions.DS9Actions(self.ds9)
            dsa.file_openas_mecube(self.cache_snapshot_fits(snapshot_id))
        return True

    def cache_size(self):
        """
        Return the current size of cached data in bytes.
        """
        count = 0
        for k, item in self._cache.items():
            count += item.size * item.itemsize
        count += self._cache_view.size * self._cache_view.itemsize
        count += self._cache_snapshot.size * self._cache_snapshot.itemsize

        return count

    def get_header_snapshot(self, view_slice):
        """
        Get the header of one slice of the cached snapshot FITS HDUList. The
        first slice with image data is the second HDU, [1] (since the [0] HDU
        is an empty PrimaryHDU). [1] also corresponds to Slice 1 in DS9 (since
        DS9 starts labelling at 1 and ignores the non-image [0] HDU).

        Does not check that the cache exists, or that slice number is valid --
        call after caching the data.
        """
        return self._cache_snapshot_fits[view_slice].header

    def get_header_view(self, snapshot_slice):
        """
        Get the header of one slice of the cached snapshot FITS HDUList. The
        first slice with image data is the second HDU, [1] (since the [0] HDU
        is an empty PrimaryHDU). [1] also corresponds to Slice 1 in DS9 (since
        DS9 starts labelling at 1 and ignores the non-image [0] HDU).

        Does not check that the cache exists, or that slice number is valid --
        call after caching the data.
        """
        return self._cache_view_fits[snapshot_slice].header

    def print_cache_size(self):
        print('Currently using %f GB RAM.' % (self.cache_size() / 1024**3))




