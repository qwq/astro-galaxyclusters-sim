"""
Useful actions to perform in DS9, automated...
"""
import contextlib
from astropy.extern.six import BytesIO


class DS9Actions:
    """
    Do stuff in DS9 using its XPA interface. The function names mimic the
    context menu flow in the DS9 program.
    """

    def __init__(self, ds9obj):
        """
        Link with a pyds9 object -- the target.
        """
        self.ds9 = ds9obj

    def file_openas_mecube(self, hdulis):
        """
        Send a HDUList object to DS9 to be opened as a datacube. This is
        almost the same as pyds9.set_fits(), but that uses 'fits' command
        which will only load the first image frame in DS9. Need 'mecube'.
        """
        with contextlib.closing(BytesIO()) as newFitsFile:
            hdulis.writeto(newFitsFile)
            newfits = newFitsFile.getvalue()
            success = self.ds9.set('mecube', newfits, len(newfits))
        return success

    def cube_goto(self, framenum):
        """
        Tell DS9 to show cube frame X.
        """
        self.ds9.set('cube %d' % framenum)

    def region_circle_create(self, units, x, y, radius, properties=""):
        """
        Create a circle.
        """
        self.ds9.set('regions system {units}; '
                     'regions command '
                     '{{circle {x} {y} {radius} # {properties}}}'.format(
                         units=units, x=x, y=y, radius=radius,
                         properties=properties))

    def region_delete_all(self):
        """
        Delete all regions.
        """
        self.ds9.set('regions delete all')

    def region_select_all(self):
        """
        Select all regions.
        """
        self.ds9.set('regions select all')

    def region_list(self):
        """
        Get the text of the region file.
        """
        return self.ds9.get('regions list')

    def zoom_pan_to(self, x, y, unit='image'):
        """
        Pan to specified coordinates.
        """
        self.ds9.set('pan to {x} {y} {unit}'.format(
            x=x, y=y, unit=unit))

    def frame_goto(self, frameno):
        """
        Go to selected frame (starts from 1). If the frame does not exist, it
        will be created. Note: frames need not be in sequence or continuous.
        """
        self.ds9.set('frame {frame}'.format(frame=frameno))

    def lock_frame(self, units='image'):
        self.ds9.set('lock frame {units}'.format(units=units))

    def lock_slice(self, units='image'):
        self.ds9.set('lock slice {units}'.format(units=units))

