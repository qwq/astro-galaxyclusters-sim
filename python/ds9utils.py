"""
Useful actions to perform in DS9, for the cluster simulation projections.
"""

import ds9actions
import numpy as np
import transform


def process_gc_center_file(filename):
    """
    Need input to be a CSV format with 4 columns; snapshot id, x, y, z
    """
    gc_peaks = np.loadtxt(filename)
    return gc_peaks


class DS9Utils(ds9actions.DS9Actions):
    """
    Show regions on top of projection images; grab regions for easier
    manipulations with directions...
    """

    def __init__(self, ds9obj, gc_center_file):
        self.gc_center = process_gc_center_file(gc_center_file)
        super().__init__(ds9obj)

    def draw_gc_circle(self, snapshot, radius, pars):
        """
        radius in resolution elements (7.4/h comoving kpc)

        Returns the image coordinates of the GC center.
        """
        gccenter = self.gc_center[snapshot][1:4]
        x, y = transform.cube_to_image([1024, 1024, 1024],
            gccenter, pars['n'], pars['binning'], pars['crop'],
            pars['cube_center'], pars['cube_r'])
        self.region_circle_create('image', x, y, radius)
        return (x, y)
