#!/usr/bin/env python3

"""
bin_to_fits.py

Convert datacube in binary stream to FITS file.

QW
2016-11-07

Changes:
2017-11-02  Refactor into form that can be used for batch processing.
"""


import sys
import struct
import numpy as np
import astropy.io.fits as pf
import os


def bin_to_fits(binfile, outfile=None):
    X, Y, Z = 1024, 1024, 1024
    TYPE, SIZE = 'f', 4

    # Default naming of output file: replace .bin suffix with .fits, or append .fits.
    if outfile is None:
        if len(binfile) > 4 and binfile[-4:] == '.bin':
            outfile = ''.join((binfile[:-4], '.fits'))
        else:
            outfile = ''.join((binfile, '.fits'))

    fh = open(binfile, 'rb')
    cube = np.zeros((Z, Y, X), dtype=np.float32)

    for k in range(Z):
        flt = fh.read(SIZE*X*Y)
        layer = struct.unpack('%d%s' % (X*Y, TYPE), flt)
        cube[k] = np.float32(layer).reshape(Y, X)

    pf.HDUList([pf.PrimaryHDU(cube)]).writeto('%s' % outfile)


def batch_bin_to_fits(infiles, outfiles, overwrite=False):
    assert(len(infiles) == len(outfiles)), 'Input and output lists must have same length'
    for x, y in zip(infiles, outfiles):
        if not os.path.isfile(x):
            print('Input file %s does not exist, skipping.' % x)
            continue
        elif os.path.isfile(y):
            if not overwrite:
                print('Output file %s exists, skipping. (overwrite=False)' % y)
                continue
            else:
                print('Output file %s is being overwritten...' % y)
        bin_to_fits(x, y)


if __name__ == '__main__':
    if len(sys.argv) not in (2, 3):
        print("Usage:")
        print("\t./bin_to_fits.py 410_T.bin [410_T.fits]")
    else:
        BINFILE = sys.argv[1]
        try:
            OUTFILE = sys.argv[2]
        except IndexError:
            OUTFILE = None
        bin_to_fits(BINFILE, OUTFILE)
    sys.exit(0)
