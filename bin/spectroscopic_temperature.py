#!/usr/bin/env python


"""
Create data cubes of spectroscopic-like temperature weights.
See Mazzotta et al. 2004.

QW
2018-08-25
"""

import sys
import astropy.io.fits as pf


def temperature_sl_weights(density, temperature):
    """
    Calculate the weight
        W = n^2 / T^0.75
    which goes into the calculation of the spectroscopic-like
    temperature,
        T_sl = int {W T} dV / int {W} dV.

    Arguments:
        density {3D numpy array} -- density
        temperature {3D numpy array} -- temperature, keV
    """
    return density**2 / temperature**0.75


def save_weights_products(nfile, tfile, outfile1, outfile2):
    """
    Save datacubes of the spectroscopic-like temperature weights, and of the
    weight*temperature product.

    To obtain a temperature map of spectroscopic-like temperature, make
    projections of both cubes and divide the second by the first.

    Arguments:
        nfile {string} -- Input density file name
        tfile {string} -- Input temperature file name
        outfile1 {string} -- Weights file name
        outfile2 {string} -- Weights * temperature file name
    """
    try:
        assert (outfile1 != tfile) and (outfile2 != tfile)
        # Don't want to overwrite input
        cubeT = pf.open(tfile)
        cuberho = pf.open(nfile)
        weights = temperature_sl_weights(cuberho[0].data,
                                         cubeT[0].data)
        weightedT = weights * cubeT[0].data

        cubeT[0].data = weights
        cubeT[0].header['CONTENT'] = 'Spectroscopic-like temperature weights'
        cubeT[0].header['ORIGFIL1'] = nfile
        cubeT[0].header['ORIGFIL2'] = tfile
        cubeT.writeto(outfile1)

        cubeT[0].data = weightedT
        cubeT[0].header['CONTENT'] = 'Spectroscopic-like weights * T'
        cubeT[0].header['ORIGFIL1'] = nfile
        cubeT[0].header['ORIGFIL2'] = tfile
        cubeT.writeto(outfile2)

    except AssertionError:
        print("Output file name cannot be the same as input file name.")


if __name__ == '__main__':
    if len(sys.argv) != 5:
        print('Usage:')
        print('\t./spectroscopic_temperature.py rho_file t_file outfile1 outfile2')
        print('(Energies in keV)')
        print('outfile1 = weights, outfile2 = weights * temperature')
    else:
        nfile, tfile = sys.argv[1], sys.argv[2]
        outfile1, outfile2 = sys.argv[3], sys.argv[4]
        save_weights_products(nfile, tfile, outfile1, outfile2)
    sys.exit(0)
