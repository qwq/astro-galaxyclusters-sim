#!/usr/bin/env python3

"""
fits_to_bin.py

Convert datacube in FITS file to binary stream.

E.g.

struct.unpack('%df'%1024*1024, txt)

Unpacks txt string as 1024*1024 floats (size 4).

A 1024x1024 numpy float32 array, img, can be converted to binary string

txt=img.reshape(1024*1024).astype('f').tostring()

Converting back and forth yields an array identical to the original.


QW
2018-08-22
"""


import sys
import struct
import numpy as np
import astropy.io.fits as pf
import os


def fits_to_bin(fitsfile, outfile=None):
    TYPE, SIZE = 'f', 4

    # Default naming of output file: replace .bin suffix with .fits, or append .fits.
    if outfile is None:
        if len(fitsfile) > 5 and fitsfile[-5:] == '.fits':
            outfile = ''.join((fitsfile[:-5], '.bin'))
        else:
            outfile = ''.join((fitsfile, '.bin'))

    fitshdus = pf.open(fitsfile)
    datacube = fitshdus[0].data
    dimensions = ','.join([str(_) for _ in datacube.shape])

    with open(outfile, 'wb') as fh:
        fh.write(datacube.flatten(order='C').astype(TYPE).tostring())
    with open('%s.dim'%outfile, 'wb') as fh2:
        fh2.write(dimensions.encode())


def batch_fits_to_bin(infiles, outfiles, overwrite=False):
    assert(len(infiles) == len(outfiles)), 'Input and output lists must have same length'
    for x, y in zip(infiles, outfiles):
        if not os.path.isfile(x):
            print('Input file %s does not exist, skipping.' % x)
            continue
        elif os.path.isfile(y):
            if not overwrite:
                print('Output file %s exists, skipping. (overwrite=False)' % y)
                continue
            else:
                print('Output file %s is being overwritten...' % y)
        fits_to_bin(x, y)


if __name__ == '__main__':
    if len(sys.argv) not in (2, 3):
        print("Usage:")
        print("\t./fits_to_bin.py 410_T.fits [410_T.bin]")
    else:
        BINFILE = sys.argv[1]
        try:
            OUTFILE = sys.argv[2]
        except IndexError:
            OUTFILE = None
        fits_to_bin(BINFILE, OUTFILE)
    sys.exit(0)
