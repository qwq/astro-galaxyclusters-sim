#!/usr/bin/env python


"""
Create data cubes of bremsstrahlung photon flux in specified energy band.

QW
2018-08-22
"""

USAGE = """
Usage:

Create emissivity data cube:
    ./bremsstrahlung.py n_file t_file outfile en_low en_high

Print value for given density and temperature in energy band:
    ./bremsstrahlung.py n T en_low en_high

(Energies in keV)
"""

import sys
import scipy.special
import astropy.io.fits as pf
import numpy as np


def bremsstrahlung(density, temperature, energy_low, energy_high):
    """
    Bremsstrahlung photon flux between two energies: integrate
        dP/dE = n^2 * T^{-0.5} * (1/E) * exp(- E / kT)
    E from from E_low to E_high

    = n^2 * T^{-0.5} * ( EI(-E_high / kT) - EI(-E_low / kT) )

    where EI is the exponential integral.

    Arguments:
        density {3D numpy array} -- density
        temperature {3D numpy array} -- temperature, keV
        energy_low {float} -- temperature, keV
        energy_high {float} -- temperature, keV
    """
    return (density**2 * temperature**(-0.5) * (
        scipy.special.expi(-energy_high / temperature) -
        scipy.special.expi(-energy_low / temperature))
    )


def save_photon_flux(density_file, temperature_file, output_file,
                     energy_low, energy_high):
    try:
        assert temperature_file != output_file  # Don't want to overwrite input
        cubeT = pf.open(temperature_file)
        cuberho = pf.open(density_file)
        datacounts = bremsstrahlung(cuberho[0].data, cubeT[0].data,
                                    energy_low, energy_high)
        cubeT[0].data = datacounts
        cubeT.writeto(output_file)
    except AssertionError:
        print("Output file name cannot be the same as input file name.")


if __name__ == '__main__':
    if len(sys.argv) not in (5, 6):
        print(USAGE)
    elif len(sys.argv) == 5:
        density, temperature = np.float64(sys.argv[1]), np.float64(sys.argv[2])
        en_low, en_high = np.float64(sys.argv[3]), np.float64(sys.argv[4])
        print(bremsstrahlung(density, temperature, en_low, en_high))
    elif len(sys.argv) == 6:
        nfile, tfile, outfile = sys.argv[1], sys.argv[2], sys.argv[3]
        en_low, en_high = np.float64(sys.argv[4]), np.float64(sys.argv[5])
        save_photon_flux(nfile, tfile, outfile, en_low, en_high)
    sys.exit(0)
