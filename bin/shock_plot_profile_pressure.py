#!/usr/bin/env python3

import sys
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import numpy as np
import re
from collections import namedtuple

matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['font.serif'] = ['Times']
matplotlib.rcParams['font.family'] = ['serif']
matplotlib.rcParams['font.size'] = 9.0
matplotlib.rcParams['axes.linewidth'] = 0.5
matplotlib.rcParams['axes.labelsize'] = 9.0
matplotlib.rcParams['axes.labelpad'] = 2.0
matplotlib.rcParams['axes.formatter.use_mathtext'] = True
matplotlib.rcParams['lines.linewidth'] = 1.0
matplotlib.rcParams['grid.linestyle'] = ':'
matplotlib.rcParams['grid.linewidth'] = 0.5
matplotlib.rcParams['grid.color'] = '#F6F6F6'
matplotlib.rcParams['xtick.direction'] = 'in'
matplotlib.rcParams['ytick.direction'] = 'in'
matplotlib.rcParams['xtick.top'] = True
matplotlib.rcParams['ytick.right'] = True
matplotlib.rcParams['xtick.major.width'] = '0.5'
matplotlib.rcParams['xtick.minor.width'] = '0.5'
matplotlib.rcParams['ytick.major.width'] = '0.5'
matplotlib.rcParams['ytick.minor.width'] = '0.5'
matplotlib.rcParams['xtick.major.size'] = '4.0'
matplotlib.rcParams['xtick.minor.size'] = '2.0'
matplotlib.rcParams['ytick.major.size'] = '4.0'
matplotlib.rcParams['ytick.minor.size'] = '2.0'
matplotlib.rcParams['legend.frameon'] = False
matplotlib.rcParams['legend.fontsize'] = 7.0
matplotlib.rcParams['savefig.bbox'] = 'tight'


if not (len(sys.argv) in (4, 6)):
    print('Usage: ./plot_profile density.dat temperature.dat outputfigure.eps')
    print('The two profiles must be extracted using the same grid.')
    exit(1)

scale = 'log'

datfilen, datfileT = sys.argv[1:3]

# The parsing of mkprof output comes from zhtools.mkprof

Profile_2D = namedtuple('Profile_2D', ['r_in', 'r_out', 'n', 'mean', 'stdev'])

with open(datfilen, 'r') as fh:
    ans = fh.read()
    ans = np.float64([
            re.sub('^\s+', '', re.sub('\s+', ' ', s)).split(' ')
            for s in ans.split('\n') if s != ''
            ])
    datan = Profile_2D(ans[:, 0], ans[:, 1], ans[:, 3], ans[:, 4], ans[:, 5])

with open(datfileT, 'r') as fh:
    ans = fh.read()
    ans = np.float64([
            re.sub('^\s+', '', re.sub('\s+', ' ', s)).split(' ')
            for s in ans.split('\n') if s != ''
            ])
    dataT = Profile_2D(ans[:, 0], ans[:, 1], ans[:, 3], ans[:, 4], ans[:, 5])


# Parsing for plotting and profile fitting from A520_fetch_profile_data
rmin = 0

pressure = datan.mean * dataT.mean


binlo = datan.r_in
binhi = datan.r_out
N = datan.n


#countmean = data.mean
# 'stdev' unused here, substitutes yerr below

valid = np.where((N > 0) * (binlo > rmin))
binlo = binlo[valid]
binhi = binhi[valid]
#countmean = countmean[valid]
#N = N[valid]
#expmean = expmean[valid]  # Not used for sim img
#fluxmean = fluxmean[valid]  # Not used for sim img

if scale == 'lin':
    grid = 0.5 * (binlo + binhi)
    xerr = 0.5 * (binhi[0] - binlo[0])  # scalar
elif scale == 'log':
    grid = (binlo * binhi)**.5
    xerr = [grid - binlo, binhi - grid]  # List of lower error, upper error
else:
    print("Scale error!")
#    return False

#yerr = (countmean * N)**.5 / (expmean * N)
#yerr = data.stdev

# return grid, fluxmean, xerr, yerr, binlo, binhi, N



# Plot settings from one of the A520 shock ipynb
x_rscale = 1
x_rmin = 0
y_scale = 1
rgrid = grid
#flux = countmean
xbinlo = binlo
xbinhi = binhi


inx = np.where(rgrid * x_rscale > x_rmin * x_rscale)
r = rgrid[inx] * x_rscale
rbinlo = xbinlo[inx] * x_rscale
rbinhi = xbinhi[inx] * x_rscale
rbins = np.append(rbinlo, rbinhi[-1])
#f = flux[inx] * y_scale
#yerr = yerr[inx] * y_scale



fig = plt.figure(figsize=(3.5, 3.5))
ax = plt.axes()


if len(sys.argv) == 6:
    plt.xlim([float(sys.argv[4]), float(sys.argv[5])])
else:
    plt.xlim([min(r) * 0.95, max(r) * 1.05])

#plt.ylim([min(f) * 0.95, max(f) * 1.05])

plt.grid(b=True, which='major', zorder=-100)
plt.grid(b=True, which='minor', zorder=-100)

# Density
plt.plot(r, datan.mean[valid] / 1e-3, color='#66ccff', zorder=100, label=r'Density / 10$^{-3}$ cm$^{-3}$')

# Temperature
plt.plot(r, dataT.mean[valid] / 1e1, color='#ffcc66', zorder=100, label=r'Temperature / 10 keV')

# Pressure
plt.plot(r, pressure[valid] / 1e-2, color='#66ff99', zorder=100, label=r'Pressure / 10$^{-2}$ cm$^{-3}$ keV', marker='.', markersize=.5, markeredgecolor='#CCCCCC')


plt.xscale('log')
plt.yscale('log')
#plt.ylabel(r'$S_X$ / cts s$^{-1}$')
plt.xlabel(r'r, px')

plt.legend()

plt.savefig(sys.argv[3])


