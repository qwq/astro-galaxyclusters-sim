#!/usr/bin/env python

'''
Project datacube into 2D
'''

import numpy as np
import astropy.io.fits as pf
import sys, os

#@profile
def project_datacube(dc, n):
	binning=2.

	MSG_E_BADNORM="Norm vector has zero length!"

	if n[0]==n[1]==n[2]==0:
		print MSG_E_BADNORM
		return False
	###
	# Normalize n
	if type(n) != np.float32:
		n=np.float32(n)
	n/=sum(a**2 for a in n)**.5

	###
	# Define image x and y vectors in datacube coordinates based on the norm
	# vector given. Norm vector is directed at the viewer along the im_z axis
	# (right-handed). 

	# Rules: 

	# If n not // to y, then im_x = y ^ n, im_y = n ^ im_x (^ means
	# cross-product); this leads to im_x, im_y aligning with x, y if n=z and
	# for n rotating about y, the y axis aligns with the image north. 

	# If n // y, then im_y = n ^ x, im_x = x; only two cases -- looking along
	# y in which case im_x = x, im_y = z, or looking down y, in which case
	# im_x = x, im_y = -z.

	if n[0]==n[2]==0: # n is along y
		imax_x=[1,0,0]
		if n[1]>0: # Looking down y
			imax_y=[0,0,-1]
		else: # Looking along y
			imax_y=[0,0,1]

	else:
		imax_x=np.cross([0,1,0],n)
		imax_x/=sum(a**2 for a in imax_x)**.5 # Normalize
		imax_y=np.cross(n,imax_x) # Unit vector (imax_x and n are orthonormal)

	imax_x=np.float32(imax_x)/binning
	imax_y=np.float32(imax_y)/binning

	###
	# Find the image offset and image size by projecting the 8 corners of the
	# datacube.

	# imoff_x, imoff_y are floor(min()) of the im_x, im_y components of the
	# corners; offsets to be subtracted.

	# imsize_x, imsize_y are ceil(max())-floor(min()) of the same.

	def map_dot_im_x(r):
		return np.dot(imax_x,r)

	def map_dot_im_y(r):
		return np.dot(imax_y,r)

	XX=dc.shape[2]
	YY=dc.shape[1]
	ZZ=dc.shape[0]

	corners_x=map(map_dot_im_x,[[XX,0,0],[0,YY,0],[0,0,ZZ],[XX,YY,0],[XX,0,ZZ],[0,YY,ZZ],[XX,YY,ZZ],[0,0,0]])
	corners_y=map(map_dot_im_y,[[XX,0,0],[0,YY,0],[0,0,ZZ],[XX,YY,0],[XX,0,ZZ],[0,YY,ZZ],[XX,YY,ZZ],[0,0,0]])

	print corners_x

	imoff_x=int(min(corners_x))
	imoff_y=int(min(corners_y))

	imsize_x=int(np.ceil(max(corners_x))-imoff_x)
	imsize_y=int(np.ceil(max(corners_y))-imoff_y)

	###
	# Project datacube. Use the center of elements. If not binning down, then
	# sub-sample by using 8 subvoxels for each voxel. Round down to get image
	# coordinates.

	image=np.zeros((imsize_y,imsize_x),np.float32)

	print imsize_x, imsize_y, imoff_x, imoff_y

	it=np.nditer(dc, flags=['multi_index'], op_flags=['readonly'])
	while not it.finished:
		x=it.multi_index[2]+.5
		y=it.multi_index[1]+.5
		z=it.multi_index[0]+.5
		image[np.floor(x*imax_y[0]+y*imax_y[1]+z*imax_y[2])-imoff_y, 
		      np.floor(x*imax_x[0]+y*imax_x[1]+z*imax_x[2])-imoff_x]+=it[0]
    	it.iternext()

	return image

def main(args):
	# Expect arguments: datafile, outputfile, n_x, n_y, n_z (need not be normalized)

	if len(args)!=6:
		print "Expect 5 arguments."
		exit()

	# Check datafile exists, and outputfile does not.

	if not os.path.isfile(args[1]):
		print "Data file does not exist"
		exit()

	if os.path.exists(args[2]):
		print "Output file exists!"
		exit()

	try:
		nx=np.float32(args[3])
		ny=np.float32(args[4])
		nz=np.float32(args[5])
	except ValueError:
		print "Bad input for normal vector components!"
		exit()

	data=pf.open(args[1])[0].data

	outimage=project_datacube(data,[nx,ny,nz])
	pf.HDUList([pf.PrimaryHDU(outimage)]).writeto(args[2])


main(sys.argv)
exit(0)


