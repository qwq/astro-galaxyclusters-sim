// progress.cc

/********************************************************
 * Display progress
 ********************************************************/

#ifndef PROGRESS_H_
#define PROGRESS_H_

#include <iostream>

void display_progress(float frac);

#endif
