// vector_math.cc

/********************************************************
 * Vector calculations.
 ********************************************************/

#ifndef VECTOR_MATH_H_
#define VECTOR_MATH_H_

#include <cmath>

double dot(double a[], double b[]);
void cross(double a[], double b[], double out[]);
double norm(double *a, int dim);
void normalize_vector(double *v, int dim);

#endif
