// datacube.h

/********************************************************
 * Read data from FITS file and write image to FITS file.
 ********************************************************/

#ifndef DATACUBE_H_
#define DATACUBE_H_

#include <cstdlib>  // malloc
#include <iostream>
#include <cstdio>  // remove
#include <fitsio.h>
#include <valarray>

class Datacube {
    public:
        Datacube(char* inputfilename);
        char* filename;
        bool CheckInit();
        std::valarray<float> GetDataPtr();
        void GetAxes(long* output);
        std::valarray<float> contents;

    private:
        long naxes[3];
        long ax1, ax2, ax3;
        bool initSuccess;
};

void cleanup_failed_output(fitsfile* outputfits, char* filename);
void readcube(char* filename, long* naxes, std::valarray<float> *data);
void printerror( int status);
void save_image(fitsfile *outputfits, std::valarray<double> *data,
    long nelements);

#endif
