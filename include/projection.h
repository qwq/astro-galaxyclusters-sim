// projection.cc

/********************************************************
 * Make projections of 3D data.
 ********************************************************/

#ifndef PROJECTION_H_
#define PROJECTION_H_

#include <iostream>
#include <valarray>
#include <omp.h>
#include "vector_math.h"
#include "progress.h"

void calculate_image_basis_vectors(double *imax_x, double *imax_y, double *n);
void calculate_slice_basis_vectors(double *imax_x, double *imax_y,
    double *n, double *ns);
void calculate_image_offset(
    long *imoff_x, long *imoff_y, long *imsize_x, long *imsize_y,
    bool crop_image, double binning, double *imax_x, double *imax_y,
    double *cube_center, double cube_r,
    double cube_xmin, double cube_xmax,
    double cube_ymin, double cube_ymax,
    double cube_zmin, double cube_zmax);

void calculate_column_length(
    long imoff_x, long imoff_y, long imsize_x, long imsize_y,
    long ax1, long ax2, long ax3,
    double *imax_x, double *imax_y, double *n, double binning,
    std::valarray<double> *projected_column);

void drizzle(std::valarray<float> *contents,
    std::valarray<double> *projected_img,
    std::valarray<long> *projected_count,
    double *imax_x, double *imax_y,
    long ax1, long ax2, long ax3,
    long imsize_x, long imsize_y, long imoff_x, long imoff_y,
    double dither_x, double dither_y, double dither_z,
    int subsample);

void drizzle2(std::valarray<float> *contents,
            std::valarray<double> *projected_img,
            std::valarray<long> *projected_count,
            double *imax_x, double *imax_y,
            long ax1, long ax2, long ax3,
            long imsize_x, long imsize_y, long imoff_x, long imoff_y,
            double dither_x, double dither_y, double dither_z,
            int subsample,
            double *cube_center, double *ns, double thickness);

#endif
