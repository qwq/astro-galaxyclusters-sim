#CC = /usr/local/opt/llvm/bin/clang -v
CC = g++
CFLAGS = -O3 -Wall -std=c++11
DEBUG = -v -g -Wall -std=c++11

#LDFLAGS = -L${CFITSIO}/lib -L${CCFITS}/CCfits/lib
#CPPFLAGS = -I/usr/local/opt/llvm/include -I${CFITSIO}/include -I${CCFITS}

SRCDIR := src
BUILDDIR := build
INCLUDEDIR := include
BINDIR := bin
TARGET := $(BINDIR)/project_datacube

SRCEXT := cc
SOURCES := $(shell find $(SRCDIR) -type f -name "*.$(SRCEXT)")
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))
INCLUDE := -I $(INCLUDEDIR)
LIBS = -lcfitsio -Xpreprocessor -fopenmp -lomp

all: $(OBJECTS) $(BINDIR)/project_datacube \
	$(BINDIR)/extract_slice \
	$(BINDIR)/extract_cone \
	$(BINDIR)/extract_cone_rbinned \
	$(BINDIR)/projection_info

#$(TARGET): $(OBJECTS)
#	@echo " Linking..."
#	@mkdir -p $(BINDIR)
#	@echo " $(CC) $^ -o $(TARGET) $(LIBS)"; $(CC) $^ -o $(TARGET) $(LIBS)

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(BUILDDIR)
	@echo " $(CC) $(CFLAGS) $(INCLUDE) -c -o $@ $< $(LIBS)"; $(CC) $(CFLAGS) $(INCLUDE) -c -o $@ $< $(LIBS)

$(BINDIR)/extract_cone: $(BUILDDIR)/vector_math.o $(BUILDDIR)/datacube.o \
	$(BUILDDIR)/progress.o $(BUILDDIR)/extract_cone.o
	$(CC) $^ -o $(BINDIR)/extract_cone $(LIBS)

$(BINDIR)/extract_cone_rbinned: $(BUILDDIR)/vector_math.o $(BUILDDIR)/datacube.o \
	$(BUILDDIR)/progress.o $(BUILDDIR)/extract_cone_rbinned.o
	$(CC) $^ -o $(BINDIR)/extract_cone_rbinned $(LIBS)

$(BINDIR)/project_datacube: $(BUILDDIR)/vector_math.o $(BUILDDIR)/datacube.o \
	$(BUILDDIR)/progress.o $(BUILDDIR)/projection.o $(BUILDDIR)/project_datacube.o
	$(CC) $^ -o $(BINDIR)/project_datacube $(LIBS)

$(BINDIR)/extract_slice: $(BUILDDIR)/vector_math.o $(BUILDDIR)/datacube.o \
	$(BUILDDIR)/progress.o $(BUILDDIR)/projection.o $(BUILDDIR)/extract_slice.o
	$(CC) $^ -o $(BINDIR)/extract_slice $(LIBS)

$(BINDIR)/projection_info: $(BUILDDIR)/projection.o $(BUILDDIR)/progress.o \
	$(BUILDDIR)/vector_math.o $(BUILDDIR)/projection_info.o
	$(CC) $^ -o $(BINDIR)/projection_info $(LIBS)

clean:
	@echo " rm -r $(BUILDDIR) $(BINDIR)/extract_cone $(BINDIR)/extract_cone_rbinned \
	$(BINDIR)/project_datacube $(BINDIR)/projection_info $(BINDIR)/extract_slice"; \
	rm -r $(BUILDDIR) \
	$(BINDIR)/extract_cone $(BINDIR)/extract_cone_rbinned \
	$(BINDIR)/project_datacube $(BINDIR)/projection_info $(BINDIR)/extract_slice
